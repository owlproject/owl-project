using UnityEngine;
using System.Collections;
using System;
using RPG;
using Algorithmics;
using Interface;


public class Enemy : MonoBehaviour {
	
	public Character enemy;
	public LoadPlayerInfos playerFunctions;
	public GenerateOrbs cubeFunctions;
	public ChangeTurn changeTurn;
	
	public GameObject lifebar;
	public GameObject owlImage;
	
	public Texture life0;
	public Texture life1;
	public Texture life2;
	public Texture life3;
	public Texture life4;
	public Texture life5;
	public Texture life6;
	public Texture life7;
	public Texture life8;
	public Texture life9;
	public Texture life10;
	
	public Texture okOwl;
	public Texture deadOwl;
	
	TextMesh enemyNameText;
	TextMesh enemyPVText;
	
	// Use this for initialization
	void Start () {
		
		lifebar = GameObject.Find("Life");
		owlImage = GameObject.Find("OwlImage");
		
		playerFunctions = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>();
		cubeFunctions = GameObject.Find("OrbGrid").GetComponent<GenerateOrbs>();
		changeTurn = GameObject.Find("Turn").GetComponent<ChangeTurn>();
		
		enemyNameText = GameObject.Find("Text : Enemy Name").GetComponent<TextMesh>();
		enemyPVText = GameObject.Find("Text : Enemy PV").GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		enemyNameText.text = enemy.name;
		enemyPVText.text = enemy.pv + " / " + enemy.pvmax;
		if(GameState.state == GameState.IA_TURN ) {
			GameState.setState(GameState.ACTION);
			playAuto();
		}
	}
	
	public void playAuto() {

		Cube cube = cubeFunctions.cube;
		IA ia = new IA (cube, enemy);
		ia.jouer();
		
		bool permutation = false;
		Coup meilleur_coup = ia.meilleur_coup;
		
		if(meilleur_coup == null) {
			Debug.Log("ERREUR: L'IA n'a pas trouvé de coup");
			return;
		} else if(meilleur_coup.shuffle) {
			GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = TextControler.newLines("The enemy shuffles the cube !", 40);
			cubeFunctions.init();
		} else if(meilleur_coup.lancer_sort) {
			meilleur_coup.spell.launch(enemy, playerFunctions.player);
			GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = TextControler.newLines(meilleur_coup.spell.description, 40);
		} else if(meilleur_coup.attaque) {
			enemy.attack(playerFunctions.player);
			GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = TextControler.newLines("The enemy attacks "+playerFunctions.player.name+" with "+enemy.weapon.name, 40);
		} else if(meilleur_coup.changer_face != 0) {
			cube.MMCube.setFaceCourante(meilleur_coup.changer_face);
			cube.majCube(meilleur_coup.changer_face);
			cubeFunctions.majGraphicCube();
			cube.play(enemy, false, -1, -1, -1, -1);
		} else {
			GameState.nextState = GameState.PLAYER_TURN;
			cubeFunctions.cube.permutation(meilleur_coup.getX1(),meilleur_coup.getY1(),meilleur_coup.getX2(),meilleur_coup.getY2());
			
			cubeFunctions.cube.play(enemy, true, meilleur_coup.getX1(),meilleur_coup.getY1(),meilleur_coup.getX2(),meilleur_coup.getY2());
			
			permutation = true;
		}
		
		if(playerFunctions.player.isDead()) {
			GameState.setState(GameState.DEFEAT);
			cubeFunctions.deleteAllOrbsFromExt(GameState.DEFEAT);
			return;
		}
		
		
		if(GameState.state != GameState.DEFEAT) {
			if(!permutation) {
				changeTurn.change(GameState.PLAYER_TURN);
			}			
		}
	}
	
	public void majBar() {
		
		GameObject.Find("Text : Enemy PV").GetComponent<TextMesh>().text = enemy.pv + " / " + enemy.pvmax;
		
		if(enemy.pv == enemy.pvmax) {
			lifebar.renderer.material.mainTexture = life10;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv>= enemy.pvmax/ 10 * 9) {
			lifebar.renderer.material.mainTexture = life9;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv>= enemy.pvmax/ 10 * 8) {
			lifebar.renderer.material.mainTexture = life8;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv>= enemy.pvmax/ 10 * 7) {
			lifebar.renderer.material.mainTexture = life7;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv>= enemy.pvmax/ 10 * 6) {
			lifebar.renderer.material.mainTexture = life6;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv>= enemy.pvmax/ 10 * 5) {
			lifebar.renderer.material.mainTexture = life5;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv>= enemy.pvmax/ 10 * 4) {
			lifebar.renderer.material.mainTexture = life4;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv>= enemy.pvmax/ 10 * 3) {
			lifebar.renderer.material.mainTexture = life3;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv>= enemy.pvmax/ 10 * 2) {
			lifebar.renderer.material.mainTexture = life2;
			owlImage.renderer.material.mainTexture = okOwl;
		} else if(enemy.pv> 0) {
			lifebar.renderer.material.mainTexture = life1;
			owlImage.renderer.material.mainTexture = okOwl;
		} else {
			lifebar.renderer.material.mainTexture = life0;
			owlImage.renderer.material.mainTexture = deadOwl;
			
			GameState.setState(GameState.VICTORY);
			cubeFunctions.deleteAllOrbsFromExt(GameState.VICTORY);
		}
	}
}

