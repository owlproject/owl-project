using UnityEngine;
using System.Collections;
using Algorithmics;
using RPG;
using Interface;

public class GraphicCube : MonoBehaviour {

	
	public GameObject cubeSidesPlane;
	public GameObject GameCube;
	
	public GameObject plane1;
	public GameObject plane2;
	public GameObject plane3;
	public GameObject plane4;
	public GameObject plane5;
	public GameObject plane6;
	public GameObject closePlane;
		
	public Vector3 topLeft;
		
	public Cube cube;	
	public GenerateOrbs generateOrbs;
	
	public ArrayList orbs = new ArrayList();
	
	void Start() {
		generateOrbs = GameObject.Find("OrbGrid").GetComponent<GenerateOrbs>();	
		createOrbs();
			
	}
	
	void  Update (){
		GameObject clickedGmObj = null;
		
		if(Input.GetMouseButtonUp(0)) {
	       	clickedGmObj = GetClickedGameObject();
	    } else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
	    	clickedGmObj = GetTouchedGameObject();
	    }
		
		int face = 0;
	
	    
	    if(clickedGmObj == GameCube) {
				initialise();	
	    }
		else if(clickedGmObj == closePlane) {
				renderObject(false);
		}
		else if(clickedGmObj == plane1) {
			face = 1;
		}
		else if(clickedGmObj == plane2) {
			face = 2;
		}
		else if(clickedGmObj == plane3) {
			face = 3;
		}
		else if(clickedGmObj == plane4) {
			face = 4;
		}
		else if(clickedGmObj == plane5) {
			face = 5;
		}
		else if(clickedGmObj == plane6) {
			face = 6;
		}
		
		if(face != 0) {
			Debug.Log("Changement vers la face " + face);
			if(cube.MMCube.getFaceCourante() == face) renderObject(false);
			else {
				cube.MMCube.setFaceCourante(face);
				cube.majCube(face);
				generateOrbs.majGraphicCube();
				renderObject(false);
				cube.play(GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>().player, false, -1, -1, -1, -1);
				GameObject.Find("Turn").GetComponent<ChangeTurn>().change(GameState.IA_TURN);
			}
		}
	    
	}	
	
	//show all 6 sides of the cube and close button
	public void initialise() {
		cube = generateOrbs.cube;
		for(int i = 1 ; i < 7 ; i++) {
			updateCubeSides(i);
		}
		
		renderObject(true);
	}
	
	void createOrbs() {
		for(int i = 1; i < 7 ; i++) {
			for(int j = 0; j < 49 ; j++) {
				
				float moveX = 1.4f;
				float moveZ = 1.4f;
				
				GameObject p = GameObject.CreatePrimitive(PrimitiveType.Plane);
				p.transform.parent = GameObject.Find("CubeSide_" + i).transform;
				p.name = "showCubeOrb_" + i + "_" + j;
				p.transform.localScale = new Vector3(0.135f, 0.135f, 0.135f);
				p.transform.localPosition = new Vector3( (4.2f - (j%7)*moveX) , 3f , (-4f + (j/7)*moveZ) );
				p.transform.Rotate(new Vector3(90, 0, 0));
				
				MeshCollider cMesh = (MeshCollider) p.GetComponent( typeof( MeshCollider) );
				cMesh.enabled = false;
				
				
				MeshRenderer pMesh = (MeshRenderer) p.GetComponent( typeof( MeshRenderer) );
				pMesh.enabled = false;
				
				p.renderer.material.shader = Shader.Find("Unlit/Transparent");
				
				orbs.Add(p);
			}
		}
	}
	
	void updateCubeSides(int side) {
		
		for(int i = 0; i < 49; i++) {
				float moveX = 1.4f;
				float moveZ = 1.4f;
				cube.MMCube.getFace(side);
				int type = cube.MMCube.getPlateau(side).getOrb(i / cube.length, i % cube.length).getType();
				
				GameObject p = GameObject.Find("showCubeOrb_" + side + "_" + i.ToString());
				
				generateOrbs.setTexture(p, type);	
			}
		
	}
	
	 public void setCurrentSide () {
			
		int side = cube.MMCube.getFaceCourante();
		
		GameObject plane = GameObject.Find("CubeSide_" + side);
		
		//Show this is the current side of the cube ??
	} 
	
	void renderObject(bool show) {
		
		MeshCollider cMesh = (MeshCollider) cubeSidesPlane.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		MeshRenderer pMesh = (MeshRenderer) cubeSidesPlane.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane1.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane1.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		
		cMesh = (MeshCollider) plane2.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane2.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane3.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane3.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane4.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane4.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane5.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane5.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane6.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane6.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) closePlane.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) closePlane.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		for(int i = 1; i < 7; i++) {
			for(int j = 0 ; j <49 ; j++) {
				GameObject p = GameObject.Find("showCubeOrb_" + i.ToString() + "_" + j.ToString());
				pMesh = (MeshRenderer) p.GetComponent( typeof( MeshRenderer) );
				pMesh.enabled = show;	
			}
		}
	}

	GameObject GetClickedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit	
		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}

	GameObject GetTouchedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
}