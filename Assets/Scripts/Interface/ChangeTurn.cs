using UnityEngine;
using System.Collections;
using RPG;

namespace Interface {
	public class ChangeTurn : MonoBehaviour {
	
		string text;
			
		public GameObject turnText;
		
		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update ()
		{
			turnText.GetComponent<TextMesh>().text = this.text;
		}
			
		public void change(int turn) {
			Debug.Log(GameState.state);
			if(GameState.state != GameState.PLAYER_TURN && GameState.state != GameState.IA_TURN && GameState.state != GameState.ACTION) return;
			this.text = "???";
			
			if(turn == GameState.PLAYER_TURN) {
				this.text = "Your turn !";
				StartCoroutine("display", turn);
			} else if(turn == GameState.IA_TURN) {
				this.text = "Enemy's turn !";
				StartCoroutine("display", turn);
			}
		}
		
		public IEnumerator display(int turn) {
			
			yield return new WaitForSeconds(1f);
			
			this.GetComponent<MeshCollider>().enabled = true;
			this.GetComponent<MeshRenderer>().enabled = true;
			turnText.GetComponent<TextMesh>().text = this.text;
			turnText.GetComponent<MeshRenderer>().enabled = true;
			
			yield return new WaitForSeconds(2f);
			
			turnText.GetComponent<MeshRenderer>().enabled = false;
			this.GetComponent<MeshRenderer>().enabled = false;
			this.GetComponent<MeshCollider>().enabled = false;
			
			yield return new WaitForSeconds(0.5f);
			
			GameState.setState(turn);
			
		}
	}
}
