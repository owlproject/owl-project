
using UnityEngine;
using System.Collections;
using Algorithmics;
using RPG;
using Interface;

public class MoveOrb : MonoBehaviour {

	public GenerateOrbs cubeFunctions;
	public GameObject gameobject;
	private bool  clickType;
	// Use this for initialization
	void Start (){
		cubeFunctions = GameObject.Find("OrbGrid").GetComponent<GenerateOrbs>();
	}
	 
	void Update (){
		if(!GameState.canPlay()) return;
		
		GameObject clickedGmObj = null;
		bool clickOccured = false;
	    
	    if(Input.GetMouseButtonUp(0)) {
	       	clickedGmObj = GetClickedGameObject();
	       	clickOccured = true;
	    } else if(Input.touchCount > 0) {
	    	clickedGmObj = GetTouchedGameObject();
	    	clickOccured = true;
	    }
	    
	    
	   	if(cubeFunctions.canMoveAgain && clickedGmObj == gameobject) {
	      	if(cubeFunctions.firstSwitch == null) {
	      		cubeFunctions.firstSwitch = clickedGmObj;
				cubeFunctions.secondSwitch = null;
	      	} else if(cubeFunctions.secondSwitch == null && gameobject != cubeFunctions.firstSwitch) {
				GameState.state = GameState.ACTION;
	      		cubeFunctions.secondSwitch = clickedGmObj;
	      		Vector3 touchCoordinates = cubeFunctions.firstSwitch.transform.position;
	      		Vector3 secondTouchCoordinates = cubeFunctions.secondSwitch.transform.position;
				
				Vector3 firstOrb = cubeFunctions.toAlgoCoords(touchCoordinates);
				Vector3 secondOrb = cubeFunctions.toAlgoCoords(secondTouchCoordinates);
				
				
				if(!cubeFunctions.cube.permutation((int) firstOrb.y, (int) firstOrb.x, (int) secondOrb.y, (int) secondOrb.x)) {
					cubeFunctions.invalidMoveFromExt();
					cubeFunctions.firstSwitch = null;
					cubeFunctions.secondSwitch = null;
					GameState.state = GameState.PLAYER_TURN;
				} else {
					GameState.nextState = GameState.IA_TURN;
					cubeFunctions.cube.play(cubeFunctions.playerFunctions.player, true, (int) firstOrb.y, (int) firstOrb.x, (int) secondOrb.y, (int) secondOrb.x);
				}
	      	}
	    }
	    
	    if(Input.touchCount > 0) {
	    	
	    	clickType = false;
	    	
		}	
	}	
		
	GameObject GetClickedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
	
	GameObject GetTouchedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}

}