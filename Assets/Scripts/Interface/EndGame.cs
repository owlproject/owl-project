﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour {
	
	public GameObject endGamePlane;
	
	public GameObject endGameTitle;
	
	public GameObject endGameExpTitle;
	public GameObject endGameExpValue;
	
	public GameObject endGameGoldTitle;
	public GameObject endGameGoldValue;
	
	public GameObject newGameButton;
	public GameObject newGameText;
	
	public GameObject mainMenuButton;
	public GameObject mainMenuText;
	
	// Use this for initialization
	void Start () {
	
	}
	
	void setEndGameTitle(string title) {
	
		endGameTitle.GetComponent<TextMesh>().text = title;
		
	}
	
	void setEndGameExpValue(string value) {
	
		endGameExpValue.GetComponent<TextMesh>().text = ""+value;
		
	}
	
	void setEndGameGoldValue(string value) {
	
		endGameGoldValue.GetComponent<TextMesh>().text = ""+value;
		
	}
	
	
	// Update is called once per frame
	void Update () {
		
		GameObject clickedGmObj = null;
		
		if(Input.GetMouseButtonUp(0)) {
	       	clickedGmObj = GetClickedGameObject();
	    } else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
	    	clickedGmObj = GetTouchedGameObject();
	    }
	    
		if(clickedGmObj == mainMenuButton) {
			Debug.Log("Open Main Menu");
		    Application.LoadLevel("mainScene");
	    }
		else if (clickedGmObj == newGameButton) {
			Debug.Log("Start New Game");
		    Application.LoadLevel("owl");
		}
	}
	
	public void renderObject(bool show, string title, int xp, int gold) {
		this.setEndGameTitle(title);
		this.setEndGameExpValue(""+xp);
		this.setEndGameGoldValue(""+gold);
		
		MeshCollider cMesh = (MeshCollider) endGamePlane.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		MeshRenderer pMesh = (MeshRenderer) endGamePlane.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) newGameButton.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) newGameButton.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		pMesh = (MeshRenderer) newGameText.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) mainMenuButton.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) mainMenuButton.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		pMesh = (MeshRenderer) mainMenuText.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		pMesh = (MeshRenderer) endGameTitle.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		pMesh = (MeshRenderer) endGameExpTitle.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		pMesh = (MeshRenderer) endGameExpValue.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		pMesh = (MeshRenderer) endGameGoldTitle.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		pMesh = (MeshRenderer) endGameGoldValue.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		
		
	}
	
	GameObject GetClickedGameObject() {
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
	
	GameObject GetTouchedGameObject() {
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
	
}
