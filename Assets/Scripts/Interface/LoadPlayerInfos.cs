﻿using UnityEngine;
using System;
using RPG;

public class LoadPlayerInfos : MonoBehaviour {
	
	public Character player;
	public Battle battle;
	
	public GameObject player_name_text;
	public GameObject pv_text;
	
	public GameObject level_text;
	public GameObject xp_text;
	
	public GameObject fire_text;
	public GameObject water_text;
	public GameObject wind_text;
	public GameObject earth_text;
	
	public GameObject current_gold_text;
	public GameObject current_xp_text;
	

	// Use this for initialization
	void Start () {
		player = new Character();
		battle = new Battle();
		
		System.Random r = new System.Random();
		int playerType = r.Next(1, 5);
		player.create(playerType);
		
		Enemy enemyFunctions = GameObject.Find ("Enemy").GetComponent<Enemy>();
		
		int enemyType = 0;
		
		do enemyType = r.Next(1, 6); 
		while (enemyType == playerType);
		
		enemyFunctions.enemy = new Character();
		enemyFunctions.enemy.create(enemyType);
		
		player_name_text.GetComponent<TextMesh>().text = player.name;
		pv_text.GetComponent<TextMesh>().text = player.pv + " / " + player.pvmax;
		
		level_text.GetComponent<TextMesh>().text = ""+player.lvl;
		xp_text.GetComponent<TextMesh>().text = player.xp + " / 100";
		
		fire_text.GetComponent<TextMesh>().text = player.fire + " / " + player.firemax;
		water_text.GetComponent<TextMesh>().text = player.water + " / " + player.watermax;
		wind_text.GetComponent<TextMesh>().text = player.wind + " / " + player.windmax;
		earth_text.GetComponent<TextMesh>().text = player.earth + " / " + player.earthmax;
		
		current_gold_text.GetComponent<TextMesh>().text = ""+battle.gold_earned;
		current_xp_text.GetComponent<TextMesh>().text = ""+battle.xp_earned;
	}
	
	// Update is called once per frame
	void Update () {
		player_name_text.GetComponent<TextMesh>().text = ""+player.name;
		pv_text.GetComponent<TextMesh>().text = player.pv + " / " + player.pvmax;
		
		level_text.GetComponent<TextMesh>().text = ""+player.lvl;
		xp_text.GetComponent<TextMesh>().text = player.xp + " / 100";
		
		fire_text.GetComponent<TextMesh>().text = player.fire + " / " + player.firemax;
		water_text.GetComponent<TextMesh>().text = player.water + " / " + player.watermax;
		wind_text.GetComponent<TextMesh>().text = player.wind + " / " + player.windmax;
		earth_text.GetComponent<TextMesh>().text = player.earth + " / " + player.earthmax;
		
		current_gold_text.GetComponent<TextMesh>().text = ""+player.gold;
		current_xp_text.GetComponent<TextMesh>().text = ""+player.xp;
	}
}
