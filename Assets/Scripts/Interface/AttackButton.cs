
using UnityEngine;
using System.Collections;
using RPG;
using Algorithmics;
using Interface;

public class AttackButton : MonoBehaviour {
	
	public LoadPlayerInfos playerFunctions;
	
	public GameObject buttonAttack;
	
	public Enemy enemy;
	
	private GenerateOrbs cubeFunctions;
	
	private ChangeTurn changeTurn;
	
	void  Start (){
		enemy = GameObject.Find("Enemy").GetComponent<Enemy>();
		
		playerFunctions = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>();
		
		changeTurn = GameObject.Find("Turn").GetComponent<ChangeTurn>();
	}
	
	void  Update (){
		
		if(!GameState.canPlay()) return;
		
		GameObject clickedGmObj = null;
		if(Input.GetMouseButtonUp(0)) {
	       	clickedGmObj = GetClickedGameObject();
	    } else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
	    	clickedGmObj = GetTouchedGameObject();
	    }
		
	    if(clickedGmObj == buttonAttack) {
			GameState.setState(GameState.ACTION);
			int pvini = enemy.enemy.pv;
			if(playerFunctions.player.attack(enemy.enemy)) {
				int degats = pvini - enemy.enemy.pv;
				GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = TextControler.newLines(playerFunctions.player.name+" attack the enemy ! -"+degats+"HP !", 40);
				StartCoroutine(ActionAnim.animateAttack());
	    		enemy.majBar();
				changeTurn.change(GameState.IA_TURN);
				
			} else {
				GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = TextControler.newLines("Not enough mana for that...", 40);
				GameState.setState(GameState.PLAYER_TURN);
			}
			
	    }
	}
	
	GameObject GetClickedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
	
	GameObject GetTouchedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
}