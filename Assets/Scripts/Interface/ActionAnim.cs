using UnityEngine;
using System.Collections;

namespace Interface {

	public class ActionAnim : MonoBehaviour {
	
		// Use this for initialization
		void Start () {
		}
		
		// Update is called once per frame
		void Update () {
			
		}
		
		public static IEnumerator animateAttack() {
			Debug.Log("Attack animation !!");
			GameObject attack = GameObject.Find("Animation : Attack");
			
			attack.GetComponent<MeshCollider>().enabled = true;
			attack.GetComponent<MeshRenderer>().enabled = true;
			
			attack.transform.parent.animation.Play("attack");
			
			yield return new WaitForSeconds(attack.transform.parent.animation["attack"].length + 0.2f);	
			
			attack.GetComponent<MeshCollider>().enabled = false;
			attack.GetComponent<MeshRenderer>().enabled = false;
		}
	}
}