
using UnityEngine;
using System.Collections;
using Interface;
using RPG;

namespace Interface {
	
public class ShuffleButton : MonoBehaviour {


	public GenerateOrbs cubeFunctions;
	public ChangeTurn changeTurn;
	
	public GameObject buttonContainer;
	
		void  Start (){
			cubeFunctions = GameObject.Find("OrbGrid").GetComponent<GenerateOrbs>();
			changeTurn = GameObject.Find("Turn").GetComponent<ChangeTurn>();
		}
		
		void  Update (){
			GameObject clickedGmObj = null;
			
			if(Input.GetMouseButtonUp(0)) {
		       	clickedGmObj = GetClickedGameObject();
		    } else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
		    	clickedGmObj = GetTouchedGameObject();
		    }
		    	
		    if(clickedGmObj == buttonContainer && GameState.state == GameState.PLAYER_TURN) {
				string player_name = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>().player.name;
				GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = TextControler.newLines(player_name+" shuffle the field !", 40);
		       	cubeFunctions.init();
				changeTurn.change(GameState.IA_TURN);
		    }
		}
		
		GameObject GetClickedGameObject (){
		    // Builds a ray from camera point of view to the mouse position
		    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		    RaycastHit hit;
		    // Casts the ray and get the first game object hit
		    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		       return hit.transform.gameObject;
		    else
		       return null;
		}
		
		GameObject GetTouchedGameObject (){
		    // Builds a ray from camera point of view to the mouse position
		    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
		    RaycastHit hit;
		    // Casts the ray and get the first game object hit
		    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		       return hit.transform.gameObject;
		    else
		       return null;
		}
	
	}
}