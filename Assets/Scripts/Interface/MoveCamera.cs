
using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {

	public int scene;

	void  Start () {
		Screen.SetResolution(Screen.width, Screen.height, true);
	}
	 
	void  Update () {
		if(Input.GetKey(KeyCode.Escape)) {
				
			switch(scene) {
				case 1 : Application.Quit();
					break;
				case 2 : Application.LoadLevel("mainScene");
					break;
			}
	       
	    }
	}
}