using System;

namespace Interface {
	
	public class TextControler {
		
		public static string newLines(string text, int tolerance) {
			string newLine = "";
			while(text.Length > tolerance) {
				int lastIndex = text.LastIndexOf(" ");
				if(lastIndex != -1) {
					newLine = text.Substring(lastIndex, text.Length - lastIndex) + newLine;
					text = text.Substring(0, lastIndex);
				} else break;
				
			}
			
			text += "\n"+newLine;
			
			return text;
		}
	}
}

