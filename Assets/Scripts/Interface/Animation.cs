using UnityEngine;
using System.Collections;

namespace Interface {

	public class Animation : MonoBehaviour {
	
		// Use this for initialization
		void Start () {
		}
		
		// Update is called once per frame
		void Update () {
			
		}
		
		public static void animateAttack() {
			Debug.Log("Attack animation");
			GenerateOrbs.log("Attaque !");
			
			GameObject attack = GameObject.Find("Animation : Attack");
			
			attack.GetComponent<MeshRenderer>().enabled = true;
			attack.GetComponent<Animation>().animation.Play("attack");
			
			attack.GetComponent<MeshRenderer>().enabled = false;
		}
	}
}