
using UnityEngine;
using System.Collections;
using RPG;
using Algorithmics;
using Interface;
using Resource;

public class Spellbook : MonoBehaviour {
	
	public GameObject open;
	public GameObject window;
	public GameObject title;
	public GameObject close;
	
	public GameObject strike; 
	
	public ChangeTurn changeTurn;
	
	GameObject[] icon = new GameObject[8];
	Spell[] spell = new Spell[8];
	bool init = false;
	int spells = 0;
		
	void Start() {
		open = GameObject.Find("Button_Spell");
		
		window = GameObject.Find("Spellbook");
		
		title = GameObject.Find("Text : Spellbook Title");
		
		close = GameObject.Find("Spellbook Close");
		
		strike = GameObject.Find("Animation : Halo");
		
		changeTurn = GameObject.Find("Turn").GetComponent<ChangeTurn>();
	}
		
	void  Update () {
		GameObject clickedGmObj = null;
		
		if(Input.GetMouseButtonUp(0)) {
	       	clickedGmObj = GetClickedGameObject();
	    } else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
	    	clickedGmObj = GetTouchedGameObject();
	    }
	
	    
	    if(clickedGmObj == open) {
			if(!init) {
				init = true;
				initialize();
			}
			render(true);	
	    }
		else if(clickedGmObj == close) {
			
			//strike.renderer.enabled = true;
			render(false);	
		} else {
			for(int i = 0; i < spells; i++) {
				if(clickedGmObj == icon[i]) {
					
					Character player = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>().player;
					Character enemy = GameObject.Find("Enemy").GetComponent<Enemy>().enemy;
					
					if(spell[i].launch(player, enemy)) {
						GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = TextControler.newLines(spell[i].replace(player.name), 40);
						render(false);
						StartCoroutine(useSpell(spell[i].heal, i, spell[i].manaColor()));
						changeTurn.change(GameState.IA_TURN);
					} else {
						GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = TextControler.newLines("Not enough mana for that...", 40);
						render(false);
						GameState.setState(GameState.PLAYER_TURN);
					}
				}
			}
		}
		
		
	    
	}
	
	public IEnumerator useSpell(bool heal, int intensity, Color color) {
			
		if ( heal ) {
			GameObject lights;
			Component halo;
			
			 for(int i = 2 ; i < 7 ; i++) {
			lights = GameObject.Find("Animation : Halo" + i.ToString());
			
			lights.light.intensity = 0;
			halo =  lights.GetComponent("Halo");
			halo.GetType().GetProperty("enabled").SetValue(halo, true, null);
			lights.light.color = Color.white;
			lights.animation.Play("heal");
			
			} 
			
			strike.light.intensity = 10;
			halo =  strike.GetComponent("Halo");
			halo.GetType().GetProperty("enabled").SetValue(halo, true, null);
			strike.light.color = Color.white;
			strike.animation.Play("heal");
			
			yield return new WaitForSeconds(strike.animation["heal"].length * strike.animation["heal"].speed + 0.1f);

			strike.light.intensity = 0;
			halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
			strike.light.color = Color.white;
			
			for(int i = 2 ; i < 7 ; i++) {
				lights = GameObject.Find("Animation : Halo" + i.ToString());
			
			lights.light.intensity = 0;
			halo = lights.GetComponent("Halo");
			halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
			lights.light.color = Color.white;
			
			}
		}
		else { 
			strike.light.intensity = intensity;
			Component halo =  strike.GetComponent("Halo");
			halo.GetType().GetProperty("enabled").SetValue(halo, true, null);
			strike.light.color = color;
			strike.animation.Play("Animation Spell");
			
			yield return new WaitForSeconds(strike.animation["Animation Spell"].length * strike.animation["Animation Spell"].speed + 0.1f);

			strike.light.intensity = 0;
			halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
			strike.light.color = Color.white;
		}
		
			
	}
	
	void initialize() {
		ArrayList sp = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>().player.spells;
		
		int i = 0;
		foreach(Spell s in sp) {
			if(i >= 8) break;
			spell[i] = s;
			createSpellObject(s, i);
			i++;
		}
		
		spells = i;
	}
	
	void createSpellObject(Spell s, int i) {
		
		GameObject parent = GameObject.Find("Spell_"+i);
		
		icon[i] = GameObject.CreatePrimitive(PrimitiveType.Plane);
		icon[i].name = "Spell_"+i+" Icon";
		icon[i].transform.position = parent.transform.position;
		icon[i].transform.Translate(new Vector3(0f, 0.3f, 0f));
		icon[i].transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
		icon[i].transform.Rotate(new Vector3(90, 0, 0));
		icon[i].renderer.material.mainTexture = s.icon;
		icon[i].renderer.material.shader = Shader.Find("Unlit/Transparent");
		
		icon[i].transform.parent = parent.transform;
		
		GameObject.Find("Spell_"+i+" Title").GetComponent<TextMesh>().text = s.name;
		
		GameObject.Find("Spell_"+i+" Fire Cost").GetComponent<TextMesh>().text = ""+s.firecost;
		GameObject.Find("Spell_"+i+" Water Cost").GetComponent<TextMesh>().text = ""+s.watercost;
		GameObject.Find("Spell_"+i+" Wind Cost").GetComponent<TextMesh>().text = ""+s.windcost;
		GameObject.Find("Spell_"+i+" Earth Cost").GetComponent<TextMesh>().text = ""+s.earthcost;
	}
	
	void render(bool show) {
		
		MeshCollider cMesh = (MeshCollider) window.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		MeshRenderer pMesh = (MeshRenderer) window.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		pMesh = (MeshRenderer) title.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		for(int i = 0; i < spells; i++) {
			
			cMesh = (MeshCollider) GameObject.Find("Spell_"+i+" Icon").GetComponent( typeof( MeshCollider) );
			cMesh.enabled = show;
			pMesh = (MeshRenderer) GameObject.Find("Spell_"+i+" Icon").GetComponent( typeof( MeshRenderer) );
			pMesh.enabled = show;
			
			pMesh = (MeshRenderer) GameObject.Find("Spell_"+i+" Title").GetComponent( typeof( MeshRenderer) );
			pMesh.enabled = show;
			
			pMesh = (MeshRenderer) GameObject.Find("Spell_"+i+" Fire Cost").GetComponent( typeof( MeshRenderer) );
			pMesh.enabled = show;
			
			pMesh = (MeshRenderer) GameObject.Find("Spell_"+i+" Water Cost").GetComponent( typeof( MeshRenderer) );
			pMesh.enabled = show;;

			pMesh = (MeshRenderer) GameObject.Find("Spell_"+i+" Wind Cost").GetComponent( typeof( MeshRenderer) );
			pMesh.enabled = show;
			
			pMesh = (MeshRenderer) GameObject.Find("Spell_"+i+" Earth Cost").GetComponent( typeof( MeshRenderer) );
			pMesh.enabled = show;
		}
		
		cMesh = (MeshCollider) close.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) close.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
	}

	GameObject GetClickedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit	
		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
	
	GameObject GetTouchedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
}