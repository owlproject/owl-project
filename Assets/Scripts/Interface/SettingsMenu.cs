﻿using UnityEngine;
using System.Collections;

public class SettingsMenu : MonoBehaviour {
	
	
	public GameObject SettingsButton;
	public GameObject closeButton;
	
	public GameObject newGameButton;	
	public GameObject mainMenuButton;
	public GameObject soundButton;
	
	public bool open = false;
	// Use this for initialization
	void Start () {
		setAudioText();
	}
	
	// Update is called once per frame
	void Update () {
		
		GameObject clickedGmObj = null;
		
		if(Input.GetMouseButtonUp(0)) {
	       	clickedGmObj = GetClickedGameObject();
	    } else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
	    	clickedGmObj = GetTouchedGameObject();
	    }
		
		if(clickedGmObj == SettingsButton) {
				StartCoroutine(renderSettingsPlane());	
	    }
		else if(clickedGmObj == closeButton) {
				StartCoroutine(renderSettingsPlane());	
	    }
		else if(clickedGmObj == mainMenuButton) {
			Debug.Log("Open Main Menu");
		    Application.LoadLevel("mainScene");
	    }
		else if (clickedGmObj == newGameButton) {
			Debug.Log("Start New Game");
		    Application.LoadLevel("owl");
		}
		else if (clickedGmObj == soundButton) {
			updateSound();
		}
		
	}
	public void setAudioText() {
		
		AudioSource audioSource = GameObject.Find("Camera").GetComponent<AudioSource>();
		GameObject soundText = GameObject.Find("Text : Sound");	
		
		if(audioSource.isPlaying) {
			soundText.GetComponent<TextMesh>().text = "Sound ON"; 
		}
		else {		
			soundText.GetComponent<TextMesh>().text = "Sound OFF"; 
		}
		
	}
	
	public void updateSound() {
		AudioSource audioSource = GameObject.Find("Camera").GetComponent<AudioSource>();
		GameObject soundText = GameObject.Find("Text : Sound");
		if (audioSource.isPlaying) {
			Debug.Log("Playing...");
			audioSource.Pause();
			soundText.GetComponent<TextMesh>().text = "Sound OFF"; 
		}
		else {
			Debug.Log("Not playing...");
			audioSource.Play();
			soundText.GetComponent<TextMesh>().text = "Sound ON"; 
		}
		
		//camera.audio = false;
	}
	
	public IEnumerator renderSettingsPlane() {
		
		GameObject plane = GameObject.Find("Settings_Plane");
		if (!open) {
			plane.animation.Play("Settings_Animation");
			yield return new WaitForSeconds(plane.animation["Settings_Animation"].length * plane.animation["Settings_Animation"].speed + 0.1f);
			open = true;
		}
		else {
			plane.animation.Play("Settings_Animation_close");
			yield return new WaitForSeconds(plane.animation["Settings_Animation_close"].length * plane.animation["Settings_Animation_close"].speed + 0.1f);
			open = false;
		}
	}
	
	GameObject GetClickedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit	
		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}

	GameObject GetTouchedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
	
}
