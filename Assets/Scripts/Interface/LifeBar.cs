
using UnityEngine;
using System.Collections;
using Interface;

public class LifeBar : MonoBehaviour {


public Texture life0;
public Texture life1;
public Texture life2;
public Texture life3;
public Texture life4;
public Texture life5;
public Texture life6;
public Texture life7;
public Texture life8;
public Texture life9;
public Texture life10;

public Texture okOwl;
public Texture deadOwl;

public GameObject lifebar;
public GameObject owlImage;

public GenerateOrbs cubeFunctions;

public int life = 100;
public int lifeMax = 100;

void Start (){
	cubeFunctions = GameObject.Find("OrbGrid").GetComponent<GenerateOrbs>();
}

void  Update (){
	GameObject clickedGmObj = null;
	if(Input.GetMouseButtonUp(0)) {
       	clickedGmObj = GetClickedGameObject();
    } else if(Input.touchCount > 0) {
    	clickedGmObj = GetTouchedGameObject();
    }
    
    if(clickedGmObj == lifebar) {
	    if(life >= 10) {
	    	life -= 10;
	    }
    	majBar();
    }
}

void  majBar (){
	if(life == lifeMax) {
		lifebar.renderer.material.mainTexture = life10;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10 * 9) {
		lifebar.renderer.material.mainTexture = life9;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10 * 8) {
		lifebar.renderer.material.mainTexture = life8;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10 * 7) {
		lifebar.renderer.material.mainTexture = life7;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10 * 6) {
		lifebar.renderer.material.mainTexture = life6;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10 * 5) {
		lifebar.renderer.material.mainTexture = life5;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10 * 4) {
		lifebar.renderer.material.mainTexture = life4;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10 * 3) {
		lifebar.renderer.material.mainTexture = life3;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10 * 2) {
		lifebar.renderer.material.mainTexture = life2;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life >= lifeMax / 10) {
		lifebar.renderer.material.mainTexture = life1;
		owlImage.renderer.material.mainTexture = okOwl;
	} else if(life > 0) {
		lifebar.renderer.material.mainTexture = life0;
		owlImage.renderer.material.mainTexture = deadOwl;
	} else {
		lifebar.renderer.material.mainTexture = life0;
		owlImage.renderer.material.mainTexture = deadOwl;
		StartCoroutine(cubeFunctions.deleteAllOrbs(1));
	}
}

GameObject GetClickedGameObject (){
    // Builds a ray from camera point of view to the mouse position
    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    RaycastHit hit;
    // Casts the ray and get the first game object hit
    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
       return hit.transform.gameObject;
    else
       return null;
}

GameObject GetTouchedGameObject (){
    // Builds a ray from camera point of view to the mouse position
    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
    RaycastHit hit;
    // Casts the ray and get the first game object hit
    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
       return hit.transform.gameObject;
    else
       return null;
}
}