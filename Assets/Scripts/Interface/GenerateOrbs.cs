using UnityEngine;
using System.Collections;
using Algorithmics;
using RPG;

namespace Interface {
	public class GenerateOrbs : MonoBehaviour {
		
		public LoadPlayerInfos playerFunctions;
		public ChangeTurn changeTurn;
		
		public const int CUBE_LENGTH = 7;
		
		public Texture fireorb;
		public Texture waterorb;
		public Texture windorb;
		public Texture earthorb;
		public Texture xporb;
		public Texture goldorb;
		
		public GameObject firstSwitch;
		public GameObject secondSwitch;
		public bool  canMoveAgain = true;
		
		public static int TOP = 1;
		public static int RIGHT = 2;
		public static int BOTTOM = 3;
		public static int LEFT = 4;
		
		public Vector3 topLeft;
			
		public ArrayList orbs = new ArrayList();
		
		// private OWL_Project.Cube cube;
		public Cube cube;
		
		void  Start (){
			playerFunctions = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>();
			changeTurn = GameObject.Find("Turn").GetComponent<ChangeTurn>();
			topLeft = new Vector3(transform.position.x + transform.localScale.x * 4.5f, transform.position.y + transform.localScale.y * 4.5f, transform.position.z);
			init();
		}
		
		void  Update () {
			
		}
		
		public void init (){
			
			cube = new Cube(CUBE_LENGTH);
			
			majGraphicCube();
			
			firstSwitch = null;
			secondSwitch = null;
			canMoveAgain = true;
			
			GameState.setState(GameState.PLAYER_TURN);

		}
		
		public void majGraphicCube() {
			
			destroyAllOrbs();
			orbs.Clear();
			
			for(int i = 0; i < cube.length * cube.length; i++) {
					
				int type = cube.getOrb(i / cube.length, i % cube.length);
				
				if(type != null && type >= 0) {
				
					GameObject p = createOrb(new Vector3(i % cube.length, i / cube.length, 0.1f), type);
					
					p.name = "Plane" + i;
		
					p.transform.parent = GameObject.Find("Plane_" + i ).transform;
					
					p.AddComponent<MoveOrb>();
					p.GetComponent<MoveOrb>().gameobject = p;
					p.GetComponent<MoveOrb>().cubeFunctions = GetComponent<GenerateOrbs>();
					p.SetActive(true);
					orbs.Add(p);
				} else {
					orbs.Add(null);	
				}
			}
		}
		
		public void destroyAllOrbs () {
			foreach(GameObject value in orbs) {
				if(value != null) {
					Destroy(value);
				}
			}
		}
			
		/*	
			Function called when objects are destroyed indicating the
			type of object, or 2 types if two different types of objects are
			destroyed at the same time. 
			TYPES :
				1) Earth
				2) Fire
				3) Water
				4) Wind
				5) Gold
				6) EXP
				
			Later we can add and number of objects destroyed -> More powerful explosion
			*/
		void DestroyObjectsEffects (GameObject firstGO) {
				
				Texture type = firstGO.renderer.material.mainTexture;
				
				type = type ? type : null;
				GameObject particle = GameObject.Find("Particle System");
				
					if(type != null) {
				particle.transform.position = firstGO.transform.position;
					switch(type.name) {
				case "earthorb" : particle.particleSystem.startColor = new Color(104, 69, 18, 1);
					break;
				case "fireorb" : particle.particleSystem.startColor = Color.red;
					break;
				case "waterorb" : particle.particleSystem.startColor = Color.blue;
					break;
				case "windorb" : particle.particleSystem.startColor = Color.white;
					break;
				case "goldorb" : particle.particleSystem.startColor = Color.yellow;
					break;
				case "xporb" : particle.particleSystem.startColor = Color.magenta;
					break;
				}
				particle.particleSystem.Emit(150);
			}	
		}			
		
		public void  setTexture ( GameObject p ,   int type  ){
			switch(type) {
				case Orb.FIRE : p.renderer.material.mainTexture = fireorb;
					break;
				case Orb.WATER : p.renderer.material.mainTexture = waterorb;
					break;
				case Orb.WIND : p.renderer.material.mainTexture = windorb;
					break;
				case Orb.EARTH : p.renderer.material.mainTexture = earthorb;
					break;
				case Orb.XP : p.renderer.material.mainTexture = xporb;
					break;
				case Orb.GOLD : p.renderer.material.mainTexture = goldorb;
					break;
			}
		}
		
		public GameObject createOrb ( Vector3 pos ,   int ran  ){
			pos = toUnityCoords(pos);
			
			GameObject p = GameObject.CreatePrimitive(PrimitiveType.Plane);
				p.transform.localScale = new Vector3(transform.localScale.x / 9, transform.localScale.y / 9, transform.localScale.z / 9);
				p.transform.position = pos;
				p.transform.Rotate(new Vector3(90, 0, 0));
				p.renderer.material.shader = Shader.Find("Unlit/Transparent");
				
				if(ran != -1) {
					setTexture(p, ran);
				}
				
				return p;
		}
		
		public void invalidMoveFromExt() {
			StartCoroutine(invalidMove());	
		}
		
		public IEnumerator invalidMove() {
			
			GameObject ParentFirst = firstSwitch.transform.parent.gameObject;
			GameObject ParentSecond = secondSwitch.transform.parent.gameObject;
			ParentFirst.animation.Play("tremble");
			ParentSecond.animation.Play("tremble");
			
			yield return new WaitForSeconds(ParentFirst.animation["tremble"].length * ParentFirst.animation["tremble"].speed + 0.1f);
		}
		
		public void switchOrbsFromExt(int direction, ArrayList orbsList) {
			StartCoroutine(switchOrbs(direction, orbsList));
		}
		
		public IEnumerator switchOrbs(int direction, ArrayList orbsList) {
			GameObject ParentFirst = firstSwitch.transform.parent.gameObject;
			GameObject ParentSecond = secondSwitch.transform.parent.gameObject;
			
			canMoveAgain = false;
			
			int firstIndex = -1;
			int secondIndex = -1;
			int counter = 0;
			
			for(int i = 0; i < orbs.Count; i++) {
				GameObject value = (GameObject) orbs[i];
				if(value != null) {
					if(value.name == firstSwitch.name) {
						value = firstSwitch;
						firstIndex = counter;
					}
					if(value.name == secondSwitch.name) {
						value = secondSwitch;
						secondIndex = counter;
					}
				}
				counter++;
			}
			
			if(firstIndex != -1 && secondIndex != -1) {
				if(firstIndex == secondIndex - 1 || firstIndex == secondIndex + 1 || firstIndex == secondIndex - cube.getTaille() || firstIndex == secondIndex + cube.getTaille()) {
					
					yield return new WaitForSeconds(0.2f);
					
					// Selon la direction vers laquelle le premier orbe permute
					switch(direction) {
						case 1 : ParentFirst.animation.Play("up");
							ParentSecond.animation.Play("down");
							break;
						case 2 : ParentFirst.animation.Play("right");
							ParentSecond.animation.Play("left");
							
							break;
						case 3 : ParentFirst.animation.Play("down");
							ParentSecond.animation.Play("up");
							break;			
						case 4 : ParentFirst.animation.Play("left");
							ParentSecond.animation.Play("right");		
							break;
					
					}
							
					yield return new WaitForSeconds(ParentFirst.animation["right"].length * ParentFirst.animation["right"].speed + 0.2f);
					
					ParentFirst.transform.position = new Vector3(0,6.400001f,0.04999977f);
					ParentSecond.transform.position = new Vector3(0,6.400001f,0.04999977f);
					Vector3 temp = firstSwitch.transform.position;
					firstSwitch.transform.position = secondSwitch.transform.position;
					secondSwitch.transform.position = temp;
					
					orbs[firstIndex] = secondSwitch;
					orbs[secondIndex] = firstSwitch;
					
					StartCoroutine(destroyOrbs(orbsList));
					
					changeTurn.change(GameState.nextState);
				} else if (ParentFirst != null && ParentSecond != null) {
					ParentFirst.animation.Play("tremble");
					ParentSecond.animation.Play("tremble");
					
					yield return new WaitForSeconds(ParentFirst.animation["tremble"].length * ParentFirst.animation["tremble"].speed + 0.1f);
				}
				
			} else {
				Debug.Log("Error : firstIndex or secondIndex not found");
			}
			
			firstSwitch = null;
			secondSwitch = null;
			
			canMoveAgain = true;
			
			
		}
		
		public void destroyOrbsFromExt(ArrayList orbs) {
			StartCoroutine(destroyOrbs(orbs));
		}
		
		public IEnumerator destroyOrbs(ArrayList orbs) {
			foreach(object o in orbs) {
				Crush go = (Crush) o;
				
				replaceOrbFromCoords(go.x, go.y, go.newType);
				
				yield return new WaitForSeconds(0.2f);
			}
		}
		
		public void replaceOrbFromCoords(int x, int y, int type) {
			deleteOrb((GameObject) orbs[x * cube.length + y], type);
		}
		
		public void deleteOrb (GameObject orb, int type) {
			for(int i = 0; i < orbs.Count; i++) {
				GameObject value = (GameObject) orbs[i];
				if(value != null && value.name == orb.name) {
					
					GameObject p = createOrb(new Vector3(i % cube.getTaille(), i / cube.getTaille(), 0.1f), type);
				
					p.name = value.name;
		
					p.transform.parent = value.transform.parent;
					
					p.AddComponent<MoveOrb>();
					p.GetComponent<MoveOrb>().gameobject = p;
					p.GetComponent<MoveOrb>().cubeFunctions = GetComponent<GenerateOrbs>();
					
					DestroyObjectsEffects(value);
					
					Destroy(value);
					
					orbs[i] = p;
				}
			}
		}
		
		public void deleteAllOrbsFromExt(int state) {
			StartCoroutine(deleteAllOrbs(state));
		}
		
		public IEnumerator deleteAllOrbs (int state){
			for(int i = 0; i < orbs.Count; i++) {
				GameObject value = (GameObject) orbs[i];
				if(value != null) {
					DestroyObjectsEffects(value);
					Destroy(value);
					value = null;
					yield return new WaitForSeconds(0.25f);
				}
			}
			
			GameState.endGame(state);
		}
		
		public Vector3 toUnityCoords(Vector3 pos){
			
			int x = (int)pos.x;
			int y = (int)pos.y;
			return new Vector3(topLeft.x - 0.1f - (x * 1.6f), 
						topLeft.y - 0.1f - (y * 1.45f), 
						pos.z);
		}
		
		public Vector3 toAlgoCoords(Vector3 pos){
			return new Vector3(-(pos.x - topLeft.x + 0.1f) / 1.6f,
						-(pos.y - topLeft.y + 0.1f) / 1.45f,
						pos.z);
		}
		
		public static void log(string msg) {
			GameObject.Find("Text : Informations").GetComponent<TextMesh>().text = msg;
		}
	}
}