using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public GameObject newGame;
	public GameObject options;
	public GameObject continueGame;
	public GameObject exit;
	
	void Update() {
		GameObject clickedGmObj = null;
		
		if(Input.GetMouseButtonUp(0)) {
	       	clickedGmObj = GetClickedGameObject();
	    } else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
	    	clickedGmObj = GetTouchedGameObject();
	    }
	    
	    if(clickedGmObj == newGame) {
			Debug.Log("New Game Clicked");
		    Application.LoadLevel("owl");
	    }
	    else if(clickedGmObj == options) {
		    Debug.Log("Options Clicked");
	    }
	    else if(clickedGmObj == exit) {
	    Debug.Log("Exit Clicked");
		    Application.Quit();
	    }
	    else if(clickedGmObj == continueGame) {
	    	Debug.Log("Continue Clicked");
	    }
	}
	
	GameObject GetClickedGameObject() {
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
	
	GameObject GetTouchedGameObject() {
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
}