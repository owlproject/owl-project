using System;
using System.Collections;
using System.IO;
using UnityEngine;
using RPG;
using Interface;

namespace Algorithmics
{
	public class IA
	{ 		
		public Cube cube;
		public Character owl;
		public Coup meilleur_coup;
		
		/**
		 * Construteur de l'IA prend un ennemi et un contexte de cube pour base de construction du meilleur coup
		 * @param pcube: cube de contexte de jeu
		 * @param owl: contient les caractéristiques du joueur représentant le personnage à faire jouer
		 * @autor : Marie Gaignebet
		 * */	
		public IA (Cube pcube, Character owl)
		{
			this.cube = pcube;
			this.owl = owl;				
		}
		
		/**
		 * Fonction contexte pour jouer
		 * @autor : Marie Gaignebet
		 * */	
		public int jouer()
		{
			meilleur_coup = new Coup();
			
			//le coup retenu est enregistré par la fonction meilleur coup comme attribut de classe
			meilleurCoup (1);
			
			if(meilleur_coup == null)
			{
				Debug.Log("IA: Aucune permutation n'est possible");
				return 1;
			}
			
			else if(meilleur_coup.shuffle)
			{
//				Debug.Log("IA: On va faire un shuffle");
				return 0;
			}
			
			else if(meilleur_coup.lancer_sort)
			{
//				Debug.Log("IA: On va lancer un sort");
				return 0;
			}
			
			return 0;
								
		}
		
		/**
		 * Fonction de construction du meilleur coup à jouer
		 * @param profondeur: servira pour un algorithme alphabeta
		 * @autor : Marie Gaignebet
		 * */
		public void meilleurCoup (int profondeur)
		{
			int best_eval = 0;
			int eval;
			meilleur_coup = new Coup();
			bool set_coup = false;
			
			//Recherche des permutations d'orbs possibles
			int length = this.cube.getTaille();
			for( int i = 0; i < length; i++)
			{
				for( int j = 0; j < length; j++)
				{
					//échange avec le haut
					if( i > 0 && best_eval < (eval = evaluation(i, j, i-1, j)))
					{
						meilleur_coup.setCoup (i,j,i-1,j,eval);
						best_eval = eval;
						set_coup = true;
					}
					
					//échange avec le bas
					if( i < length - 1 && best_eval < (eval = evaluation(i, j, i+1, j)))
					{
						meilleur_coup.setCoup (i,j,i+1,j,eval);
						best_eval = eval;
						set_coup = true;
					}
					
					//échange avec la gauche
					if( j > 0 && best_eval < (eval = evaluation(i, j, i, j-1)))
					{
						meilleur_coup.setCoup (i,j,i,j-1,eval);
						best_eval = eval;
						set_coup = true;
					}
					
					//échange avec la droite
					if( j < length - 1 && best_eval < (eval = evaluation(i, j, i, j+1)))
					{
						meilleur_coup.setCoup (i,j,i,j+1,eval);
						best_eval = eval;
						set_coup = true;
					}
				}
			}
			
			//si pas de permutations possibles, on peut envisager la regénération du cube
			if(!set_coup)
			{ 
				eval = evaluationShuffle(); //priorité 0
				if(best_eval < eval)
				{
					meilleur_coup.setCoupShuffle(eval);
					best_eval = eval;
				}
			}
			
			//évaluation d'un changement de face
			for(int face = 1; face <= 6; face++)
			{	
				if(face != cube.MMCube.getFaceCourante())
				{
					eval = evaluationChangerFace(face);//priorité 1
					if(best_eval < eval)
					{
						meilleur_coup.setCoupFace(eval, face);
						best_eval = eval;
					}
				}
			}
			
			//évaluation de l'attaque
			eval = evaluationAttaque();//priorité 3
			if(best_eval < eval)
			{
				meilleur_coup.setCoupAttaque(eval);
				best_eval = eval;
			}
			
			//évaluation du lancement d'un sort
			int cpt = 0;
			ArrayList liste_sorts = new ArrayList();
			for(int i = 0; i < owl.spells.Count; i++) {
				Spell s = (Spell) owl.spells[i];
				
				if(owl.canCast(s))
				{
					cpt++;
					liste_sorts.Add(s);
//						eval = evaluationSort();//priorité 4
//						if(best_eval < eval)
//						{
//							meilleur_coup.setCoupSort(eval);
//							best_eval = eval;
//						}	
				}
			}	
			eval = cpt*10;
			if(best_eval < eval)
			{
				System.Random ran = new System.Random();
				int random = ran.Next(0, liste_sorts.Count);
				meilleur_coup.setCoupSort(eval, (Spell) liste_sorts[random]);
				best_eval = eval;
			}		
		}
		
		/**Fonction de détection de combinaison
		 * @param x: absice de l'orb 1
		 * @param y: ordonnée de l'orb 1
		 * @param x: absice de l'orb 2
		 * @param y: ordonnée de l'orb 2
		 * * @autor : Marie Gaignebet
		 * */
		public Coup combinaison(int x, int y, int xx, int yy) {
			
			int type;
			int valide;
			
			//enregistrement des impacts du coup
			type = this.cube.getOrb(x,y);			
			int g1 = gauche(x,y,type);
			int d1 = droite(x,y,type);
			int h1 = haut(x,y,type);
			int b1 = bas(x,y,type);
			valide = g1+d1+h1+b1;
			
			int g2 = gauche(x,y,type);
			int d2 = droite(x,y,type);
			int h2 = haut(x,y,type);
			int b2 = bas(x,y,type);
			valide += g2+d2+h2+b2;
						
			//enregistre et renvoie le coup s'il est valide
			if(valide == 0) return null;
			else
				return new Algorithmics.Coup(d1,d2,g1,g2,h1,h2,b1,b2);
			
		}
		
		/*
		 * Recherche de la portée des combinaisons sur la droite d'un orb
		 * @param x: absice de l'orb 
		 * @param y: ordonnée de l'orb 
		 * @param type: type de l'orb 
		 * @return : portée du coup
		 * @autor : Marie Gaignebet
		 * */
		public int droite(int x, int y, int type) {
			int i = y + 1;
			int droite = 1;
			int length = this.cube.getTaille();
			while(i < length && type == cube.getOrb(x, i)) {
				droite++;
				i++;
			}
			
			if(droite >= 3) return droite;
			else  			return 0;
		}
		
		/*
		 * Recherche de la portée des combinaisons sur la gauche d'un orb
		 * @param x: absice de l'orb 
		 * @param y: ordonnée de l'orb 
		 * @param type: type de l'orb 
		 * @return : portée du coup
		 * @autor : Marie Gaignebet
		 * */
		public int gauche(int x, int y, int type) {
			int i = y - 1;
			int gauche = 1;
			int length = this.cube.getTaille();
			while(i >= 0 && type == cube.getOrb(x, i)) {
				gauche++;
				i--;
			}
			
			if(gauche >= 3) return gauche;
			else  			return 0;
		}
		
		/*
		 * Recherche de la portée des combinaisons sur le haut d'un orb
		 * @param x: absice de l'orb 
		 * @param y: ordonnée de l'orb 
		 * @param type: type de l'orb 
		 * @return : portée du coup
		 * @autor : Marie Gaignebet
		 * */
		public int haut(int x, int y, int type) {
			int i = x - 1;
			int haut = 1;
			int length = this.cube.getTaille();
			while(i >= 0 && type == cube.getOrb(i, y)) {
				haut++;
				i--;
			}
			
			if(haut >= 3) return haut;
			else  		  return 0;
		}
		
		/*
		 * Recherche de la portée des combinaisons sur le bas d'un orb
		 * @param x: absice de l'orb 
		 * @param y: ordonnée de l'orb 
		 * @param type: type de l'orb 
		 * @return : portée du coup
		 * @autor : Marie Gaignebet
		 * */
		public int bas(int x, int y, int type) {
			int i = x + 1;
			int bas = 1;
			int length = this.cube.getTaille();
			while(i < length && type == cube.getOrb(i, y)) {
				bas++;
				i++;
			}
			
			if(bas >= 3) return bas;
			else  		 return 0;
		}
		
		/*
		 * Evalue un coup à jouer
		 * @param x: absice de l'orb 1
		 * @param y: ordonnée de l'orb 1
		 * @param x: absice de l'orb 2
		 * @param y: ordonnée de l'orb 2
		 * @return : valeur du coup
		 * @autor : Marie Gaignebet
		 * */
		public int evaluation(int x, int y, int xx, int yy)
		{		
			//applique temporairement le coup pour juger des impacts sur le plateau
			permuteTypesTemporaire(x, y, xx, yy);
			Coup coup = combinaison(x,y,xx,yy);
			
			if(coup == null)
			{
				permuteTypesTemporaire(x, y, xx, yy);
				return -1;
			}
			
			int valeur = 0;
			//enregistre le mana gagné pour chaque catégorie
			int[] mana = new int[6] {0,0,0,0,0,0};// feu = 0, eau = 0, terre = 0, air = 0, xp = 0, or = 0;
			
			int abs = x, ord = y;
			int type;
			
			//droite
			int max = coup.d1;
			if(coup.d2 > coup.d1) max = coup.d2; 
			while(abs <= max) 
			{
				type = transfoTypeToIndex(cube.getOrb(abs, y));
				mana[type] = mana[type]++;
				abs++;
			}
			
			//gauche
			abs = x;
			max = coup.g1;
			if(coup.g2 > coup.g1) max = coup.g2; 
			while(abs >= max) 
			{			
				type = transfoTypeToIndex(cube.getOrb(abs, y));
				mana[type] = mana[type]++;
				abs--;
			}
			
			//bas
			ord = y;
			max = coup.b1;
			if(coup.b2 > coup.b1) max = coup.b2; 
			while(ord <= max) 
			{
				type = transfoTypeToIndex(cube.getOrb(x, ord));
				mana[type] = mana[type]++;
				ord++;
			}
			
			//haut
			ord = y;
			max = coup.h1;
			if(coup.h2 > coup.h1) max = coup.h2; 
			while(ord >= max) 
			{
				type = transfoTypeToIndex(cube.getOrb(x, ord));
				mana[type] = mana[type]++;
				ord--;
			}
			
			//valeur augmente avec nombre orb detruits
			valeur += coup.b1 + coup.b2 + coup.d1+ coup.d2+ coup.g1+ coup.g2+ coup.h1+ coup.h2;
			
			//valeur aumente quand on récupère un mana qui tend vers 10 ou plus
			for(int var = 0; var < 6 ; var ++)
			{
				valeur += evaluationMana(var+1, mana[var]);
			}
				
			//annule la permutation
			permuteTypesTemporaire(x, y, xx, yy);
			return valeur*20;
		}
		
		/**
		 * Transforme un type d'orb en index
		 * @param type: type a convertir
		 * @return : valeur de l'index
		 * @autor : Marie Gaignebet
		 * */
		public int transfoTypeToIndex(int type)
		{
			return type -1;
		}
		
		/*
		 * Permute deux orbs dans le plateau (en échangeant juste deux types, pas d'impact sur le cube profond)
		 * @param x: absice de l'orb 1
		 * @param y: ordonnée de l'orb 1
		 * @param x: absice de l'orb 2
		 * @param y: ordonnée de l'orb 2
		 * @author: Marie Gainebet
		 * */
		public void permuteTypesTemporaire(int x, int y, int xx, int yy)
		{			
			int type = this.cube.getOrb(x, y);
			this.cube.setType(x, y, this.cube.getOrb(xx,yy));
			this.cube.setType(xx, yy, type);
		}
		
		/*
		 * Evalue une regénération du cube
		 * Amélioration à apporter: enregistrer le shuffle car c'est aléatoire: il faudrait enregitrer le cube regénéré
		 * @return : valeur du shuffle
		 * @autor : Marie Gaignebet
		 * */
		public int evaluationShuffle()
		{				
//			Cube cube_shuffle = new Cube(this.cube.length);
//			int[] mana = new int[6] {0,0,0,0,0,0};
//			int type = 0;
			int valeur = 0;
//
//			for(int i = 0; i < cube_shuffle.length; i++) {
//				for (int j = 0; j < cube_shuffle.length; j++) {
//					type = transfoTypeToIndex(cube_shuffle.getOrb(i, j));
//					mana[type] = mana[type]++;
//				}
//			}
//				
//			for(int var = 0; var < 6 ; var ++)
//			{
//				valeur += evaluationMana(var+1, mana[var]);
//			}	
			
			valeur = 10;
			return valeur;//et attribuer un cube au coup
		}
		
		/*
		 * Evalue un chanement de face
		 * @param face: numéro de la face à évaluer
		 * @return : valeur du changement
		 * @autor : Marie Gaignebet
		 * */
		public int evaluationChangerFace(int face)
		{	
			cube.MMCube.getFace(face);
			OWL_Project.Plateau pla = cube.MMCube.getPlateau(face);
			
			//enregistre le contenu de la face sur laquelle on va travailler
//			Orb[,] cube_fake = new Orb[cube.length, cube.length];
//			for(int i = 0; i < cube.length; i++) {
//				for (int j = 0; j < cube.length; j++) {
//					cube_fake[i,j] = new Orb(pla.getOrb(i,j).getType());
//				}
//			}
				
			int[] mana = new int[6] {0,0,0,0,0,0};
			int type = 0;
			int valeur = 0;

			for(int i = 0; i < cube.length; i++) {
				for (int j = 0; j < cube.length; j++) {
					type = transfoTypeToIndex(pla.getOrb(i,j).getType());
					mana[type] = mana[type]++;
				}
			}
				
			//évalue l'apport des nouveaux mana
			for(int var = 0; var < 6 ; var ++)
			{
				valeur += evaluationMana(var+1, mana[var]);
			}	
			
			return valeur;
		}
		
		/*
		 * Evalue une attaque
		 * @return : valeur de l'attaque
		 * @autor : Marie Gaignebet
		 * */
		public int evaluationAttaque()
		{
			int valeur = 0;
			if(owl.canAttack())
			{
				Character player = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>().player;
				int vie = player.pv;
				valeur = 100;
				int puissance = player.weapon.fireattackmax + player.weapon.earthattackmax + player.weapon.windattackmax + player.weapon.waterattackmax;
				int mana_util = player.weapon.firecost + player.weapon.earthcost + player.weapon.windcost + player.weapon.watercost;
				
				//valeur est égale au ratio de la puissance et du mana requis pour lancer le coup
				valeur =  30+ 100 * (int) Math.Floor((double)puissance/(double) mana_util);
				
				//on accorde plus d'importance si la vie est basse
				if(vie < puissance) valeur *= 20;
			}
			else
				valeur = -100;
			return valeur;
		}
		
		/*
		 * Evalue un sort
		 * @param s: sort à évaluer
		 * @return : valeur du sort
		 * @autor : Marie Gaignebet
		 * */
		public int evaluationSort(Spell s)
		{
			int valeur = 0;
			int mana = s.firecost + s.windcost + s.watercost + s.earthcost;
			
			//valeur si le sort est une attaque
			if(s.attack)
			{
				valeur = s.fireattack + s.windattack + s.waterattack + s.earthattack;
				Character player = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>().player;
					
				//on accorde plus d'importance si la vie est basse
				if(player.pv < valeur) valeur *= 20; 
			}
			//valeur si le sort est un soin
			else{
				valeur = s.fireheal + s.windheal + s.waterheal + s.earthheal;
				if(owl.shouldHeal()) valeur *= 2;
			}
			
			//valeur est aussi un ratio des bienfaits sur le mana dépensé
			valeur = (int) Math.Floor((double)valeur/(double)mana);
			
			return valeur;
		}
		
		/*
		 * Evalue l'apport d'un mana pour un joueur donné (OWL)
		 * @param type: type de l'orb à évaluer
		 * @param type: nombre d'orbs de ce type à évaluer
		 * @return : valeur du sort
		 * @autor : Marie Gaignebet
		 * */
		public int evaluationMana(int type, int nombre)
		{
			int FIRE = 1;
			int WATER = 2;
			int EARTH = 3;
			int WIND = 4;
			int lim;
			int min, moy, max;
			int valeur = 0;
			
			//la valeur augmente si le joueur est du meme élément que le mana que l'on évalue
			int type_owl = 0;
			if(owl.name == "Kram") type_owl = FIRE;
			else if(owl.name == "Ameri") type_owl = WATER;
			else if(owl.name == "Brodomir") type_owl = EARTH;			
			else if(owl.name == "Ventes") type_owl = WIND;
			
			if(type_owl == type) valeur += 30;
			if(type == 4) valeur += 2;
			if(type == 5) valeur += 2;
			else if(owl.name == "Enima" && type >= 4) valeur += 20;
			
			
			/*on attribue une valeur selon si:
			 * -on a une valeur minimale de mana
			 * -on a une valeur moyenne de mana
			 * -on a une grande valeur de mana
			 * -on a déjà récupéré un maximum de mana
			 **/			
			switch(type) {
				case 1 : // FIRE CHARACTER
					lim = owl.firemax;
					min = (int) Math.Floor(lim/5f);
					moy = (int) Math.Floor(lim*2f/5f);
					max = (int) Math.Floor(lim*4f/5f);
				
					if(owl.fire == lim){
						valeur = 0;
						return valeur;
					}
				
					if(nombre == lim) valeur += lim*10;
					if(nombre > max) valeur += max*10;
					else if(nombre > moy) valeur += moy*10;
					else valeur += min;
					
				
					lim = owl.watermax;
					min = (int) Math.Floor(lim/5f);
					moy = (int) Math.Floor(lim*2/5f);
					max = (int) Math.Floor(lim*4/5f);
				
					if(owl.water == lim){
						valeur = 0;					
						return valeur;
					}
				
					if(nombre == lim) valeur += lim*10;
					if(nombre > max) valeur += max*10;
					else if(nombre > moy) valeur += moy*10;
					else valeur += min;
	
				break;
				
				case 2 : // WATER CHARACTER
					lim = owl.firemax;
					min = (int) Math.Floor(lim/5f);
					moy = (int) Math.Floor(lim*2/5f);
					max = (int) Math.Floor(lim*4/5f);
				
					if(owl.fire == lim){
						valeur = 0;
						return valeur;
					}
				
					if(nombre == lim) valeur += lim*10;
					if(nombre > max) valeur += max*10;
					else if(nombre > moy) valeur += moy*10;
					else valeur += min;
					
				
					lim = owl.watermax;
					min = (int) Math.Floor(lim/5f);
					moy = (int) Math.Floor(lim*2/5f);
					max = (int) Math.Floor(lim*4/5f);
				
					if(owl.water == lim){
						valeur = 0;
						return valeur;
					}
				
					if(nombre == lim) valeur += lim*10;
					if(nombre > max) valeur += max*10;
					else if(nombre > moy) valeur += moy*10;
					else valeur += min;
				break;
				
				case 3 : // EARTH CHARACTER
					lim = owl.earthmax;
					min = (int) Math.Floor(lim/5f);
					moy = (int) Math.Floor(lim*2/5f);
					max = (int) Math.Floor(lim*4/5f);
				
					if(owl.fire == lim){
						valeur = 0;
						return valeur;
					}
				
					if(nombre == lim) valeur += lim*10;
					if(nombre > max) valeur += max*10;
					else if(nombre > moy) valeur += moy*10;
					else valeur += min;
					
				
					lim = owl.windmax;
					min = (int) Math.Floor(lim/5f);
					moy = (int) Math.Floor(lim*2/5f);
					max = (int) Math.Floor(lim*4/5f);
				
					if(owl.water == lim){
						valeur = 0;
						return valeur;
					}
				
					if(nombre == lim) valeur += lim*10;
					if(nombre > max) valeur += max*10;
					else if(nombre > moy) valeur += moy*10;
					else valeur += min;
				break;
				
				case 4 : // WIND CHARACTER
					lim = owl.earthmax;
					min = (int) Math.Floor(lim/5f);
					moy = (int) Math.Floor(lim*2/5f);
					max = (int) Math.Floor(lim*4/5f);
				
					if(owl.fire == lim){
						valeur = 0;
						return valeur;
					}
				
					if(nombre == lim) valeur += lim*10;
					if(nombre > max) valeur += max*10;
					else if(nombre > moy) valeur += moy*10;
					else valeur += min;
					
				
					lim = owl.windmax;
					min = (int) Math.Floor(lim/5f);
					moy = (int) Math.Floor(lim*2/5f);
					max = (int) Math.Floor(lim*4/5f);
				
					if(owl.water == lim){
						valeur = 0;
						return valeur;
					}
				
					if(nombre == lim) valeur += lim*10;
					if(nombre > max) valeur += max*10;
					else if(nombre > moy) valeur += moy*10;
					else valeur += min;
				break;
			}
			
			valeur += nombre;
			return (int) Math.Floor((double)valeur*(double)nombre/10f);
		}
	}
}

