using UnityEngine;
using System.Collections;

namespace RPG {
	
	public class Battle {
	
		private const int PLAYER1_TURN = 1;
		private const int PLAYER2_TURN = 0;
		
		private const int PLAYER1_VICTORY = 1;
		private const int PLAYER2_VICTORY = 2;
		private const int SURRENDER = 3;
		
		private Character player1;
		private Character player2;
		
		//private Cube cube;
		
		private int turn;
		private int orb_destroyed;
		private int damage_dealt;
		private int biggest_attack;
		private int level_ia; // 3 constants to create for an IA object : EASY, MEDIUM and HARD
		
		public float xp_bonus; // We set a multiplier given by the difficulty of the level ( 1 default  )
		public float gold_bonus; // We set a multiplier given by the difficulty of the level ( 1 default  )
		
		public int xp_earned;
		public int gold_earned;
		
		public void create(Character player1, Character player2, int difficulty) {
			xp_earned = 0;
			gold_earned = 0;
			turn = 1;
			orb_destroyed = 0;
			damage_dealt = 0;
			biggest_attack = 0;
			level_ia = difficulty;
			
			this.player1 = player1;
			this.player2 = player2;
			
			switch(level_ia) { // TODO: Remplacer 1 par Ia.EASY, 2 par Ia.MEDIUM et 3 par Ia.HARD
				case 1 : // EASY
					xp_bonus = 1;
					gold_bonus = 1;
				break;
				
				case 2 : // MEDIUM
					xp_bonus = 1.5f;
					gold_bonus = 1.2f;
				break;
				
				case 3 : // HARD
					xp_bonus = 2;
					gold_bonus = 1.5f;
				break;
				
				default :
					xp_bonus = 1;
					gold_bonus = 1;
				break;
			}
			
		}
		
		public void startBattle() {
			 while(!player1.isDead() && !player2.isDead()) {
				playTurn();
				turn++;
				
			}
			
			if(!player1.isDead()) {
				endBattle(PLAYER2_VICTORY);
			}
			
			endBattle(PLAYER1_VICTORY);
		}
		
		public void  endBattle ( int state  ){
			switch(state) {
				case PLAYER1_VICTORY :
					player1.xp += (int)(xp_earned * xp_bonus);
					player1.gold += (int)(gold_earned * gold_bonus);
					// GO écran resultat (victoire joueur 1)
				break;
				
				case PLAYER2_VICTORY :
					player2.xp += (int)(xp_earned * xp_bonus);
					player2.gold += (int)(gold_earned * gold_bonus);
					// GO écran resultat (victoire joueur 2)
				break;
				
				case SURRENDER :
					// Just Quit back to the main screen
				break;
				
			}
		}
		
		public void  playTurn (){
			if(turn % 2 == PLAYER1_TURN) {
				//player1.action();
			} else {
				//player2.action();
				Debug.Log("TEST PRO IA");
			}
		}
		
	}
}