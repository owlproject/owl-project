using System;
using UnityEngine;

namespace RPG {
	public class GameState {
		
		public const int PLAYER_TURN = 1;
		public const int IA_TURN = 2;
		public const int ACTION = 3;
		public const int PAUSE = 4;
		public const int VICTORY = 5;
		public const int DEFEAT = 6;
		
		public static int state = PLAYER_TURN;
		public static int nextState = IA_TURN;
		
		public static bool canPlay() {
			return state == PLAYER_TURN;
		}
		
		public static void setState(int state) {
			GameState.state = state;
		}
		
		public static void endGame(int state) {
			string title = "";
			
			if(state == GameState.VICTORY) {
				title = "Victory !";
			}
			
			if(state == GameState.DEFEAT) {
				title = "Defeat...";
			}
			
			LoadPlayerInfos infos = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>();
			
			GameObject.Find("End Game").GetComponent<EndGame>().renderObject(true, title, infos.player.xp, infos.player.gold);
			infos.player.xp += infos.battle.xp_earned;
			infos.player.gold += infos.battle.gold_earned;
		}
	}
}

