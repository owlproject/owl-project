
using UnityEngine;
using System.Collections;

namespace RPG {
	
	public class Weapon {
		public string name = "";
		public int goldcost = 0;
		
		public int fireattack = 0;
		public int waterattack = 0;
		public int earthattack = 0;
		public int windattack = 0;
		
		public int fireattackmax = 0;
		public int waterattackmax = 0;
		public int earthattackmax = 0;
		public int windattackmax = 0;
		
		public int firecost = 0;
		public int watercost = 0;
		public int earthcost = 0;
		public int windcost = 0;
		
		
		public void create(string name, int goldcost, int fireattack, int waterattack, int earthattack, int windattack, 
													int fireattackmax, int waterattackmax, int earthattackmax, int windattackmax, 
													int firecost, int watercost, int earthcost, int windcost) {
			
			this.name = name;
			this.goldcost = goldcost;
			
			this.fireattack = fireattack;
			this.waterattack = waterattack;
			this.earthattack = earthattack;
			this.windattack = windattack;
			
			this.fireattackmax = fireattackmax;
			this.waterattackmax = waterattackmax;
			this.earthattackmax = earthattackmax;
			this.windattackmax = windattackmax;
			
			this.firecost = firecost;
			this.watercost = watercost;
			this.earthcost = earthcost;
			this.windcost = windcost;
		}
		
		public void create() {
			this.name = "Epee en bois";
			this.goldcost = 5;
			
			this.fireattack = 0;
			this.waterattack = 0;
			this.earthattack = 2;
			this.windattack = 0;
			
			this.fireattackmax = 0;
			this.waterattackmax = 0;
			this.earthattackmax = 3;
			this.windattackmax = 0;
			
			this.firecost = 0;
			this.watercost = 0;
			this.earthcost = 3;
			this.windcost = 0;
		}
	}
}