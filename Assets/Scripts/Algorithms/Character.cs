using System;
using UnityEngine;
using System.Collections;
using Resource;

namespace RPG {
	public class Character {
		public const int FIRE = 1;
		public const int WATER = 2;
		public const int EARTH = 3;
		public const int WIND = 4;
		public const int NEUTRAL = 5;
		
		public string name = "";
		public int lvl = 1;
		public int xp = 0;
		public int gold = 0;
		public int pv = 0;
		public int pvmax = 0;
		
		public int fire = 0;
		public int water = 0;
		public int earth = 0;
		public int wind = 0;
		
		public int firemax = 0;
		public int watermax = 0;
		public int earthmax = 0;
		public int windmax = 0;
		
		public int firecollect = 0;
		public int watercollect = 0;
		public int earthcollect = 0;
		public int windcollect = 0;
		
		public int fireresist = 0;
		public int waterresist = 0;
		public int earthresist = 0;
		public int windresist = 0;
		
		public int fireattack = 0;
		public int waterattack = 0;
		public int earthattack = 0;
		public int windattack = 0;
		
		public Weapon weapon;
		
		public ArrayList spells;
		
		public void create(int type) {
			
			this.lvl = 1;
			this.pv = 42;
			this.pvmax = 42;
			
			this.firemax = 10;
			this.watermax = 10;
			this.earthmax = 10;
			this.windmax = 10;
			
			this.firecollect = 0;
			this.watercollect = 0;
			this.earthcollect = 0;
			this.windcollect = 0;
			
			this.fireresist = 100;
			this.waterresist = 100;
			this.earthresist = 100;
			this.windresist = 100;
			
			this.fireattack = 100;
			this.waterattack = 100;
			this.earthattack = 100;
			this.windattack = 100;
			
			spells = new ArrayList();
			Spell s;
			
			Textures t = GameObject.Find("Textures").GetComponent<Textures>();
		
			switch(type) {
				case FIRE : // FIRE CHARACTER
					this.name = "Kram";
					this.firemax = 12;
					this.firecollect = 1;
					this.fireresist = 90;
					this.fireattack = 110;
					
					this.watermax = 8;
					this.watercollect = -1;
					this.waterresist = 110;
					this.waterattack = 90;
					
					this.weapon = new Weapon();
					weapon.create("Wooden stick", 5, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0);
				
					s = new Spell("Life branch", t.heal, 1, 0, 0, 5, 0);
					s.setDescription("/name feel better !");
					s.addHealProperty(0, 0, 6, 0);
					spells.Add(s);
				
					s = new Spell("Burning touch", t.heal, 1, 12, 0, 0, 0);
					s.setDescription("/name touch the enemy, who is now burning !");
					s.addDamages(12, 0, 0, 0);
					spells.Add(s);
				break;
				
				case WATER : // WATER CHARACTER
					this.name = "Ameri";
					this.watermax = 12;
					this.watercollect = 1;
					this.waterresist = 90;
					this.waterattack = 110;
					
					this.firemax = 8;
					this.firecollect = -1;
					this.fireresist = 110;
					this.fireattack = 90;
					
					this.weapon = new Weapon();
					weapon.create("Rusty trident", 5, 0, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0);
				
					s = new Spell("Calming breeze", t.heal, 1, 0, 0, 0, 5);
					s.setDescription("/name feel better !");
					s.addHealProperty(0, 0, 0, 6);
					spells.Add(s);
				
					s = new Spell("Waves", t.heal, 1, 0, 12, 0, 0);
					s.setDescription("A gigantic wave crush the enemy !");
					s.addDamages(0, 12, 0, 0);
					spells.Add(s);
				break;
				
				case EARTH : // EARTH CHARACTER
					this.name = "Brodomir";
					this.earthmax = 12;
					this.earthcollect = 1;
					this.earthresist = 90;
					this.earthattack = 110;
					
					this.windmax = 8;
					this.windcollect = -1;
					this.windresist = 110;
				
					this.windattack = 90;
					
					this.weapon = new Weapon();
					weapon.create("Cave mass", 5, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0);
				
					s = new Spell("Sweet heat", t.heal, 1, 5, 0, 0, 0);
					s.setDescription("/name feel better !");
					s.addHealProperty(6, 0, 0, 0);
					spells.Add(s);
				
					s = new Spell("Rock you", t.heal, 1, 0, 0, 12, 0);
					s.setDescription("/name throw a big rock on the enemy");
					s.addDamages(0, 0, 12, 0);
					spells.Add(s);
				break;
				
				case WIND : // WIND CHARACTER
					this.name = "Ventes";
					this.windmax = 12;
					this.windcollect = 1;
					this.windresist = 90;
					this.windattack = 110;
					
					this.earthmax = 8;
					this.earthcollect = -1;
					this.earthresist = 110;
					this.earthattack = 90;
					
					this.weapon = new Weapon();
					weapon.create("Inaccurate bow", 5, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 3);
				
					s = new Spell("Calm waters", t.heal, 1, 0, 5, 0, 0);
					s.setDescription("/name feel better !");
					s.addHealProperty(0, 6, 0, 0);
					spells.Add(s);
				
					s = new Spell("Sound waves", t.heal, 1, 0, 0, 0, 12);
					s.setDescription("/name scream so hard he break the enemy's eardrums");
					s.addDamages(0, 0, 0, 12);
					spells.Add(s);
				break;
				
				default : // NORMAL CHARACTER
					this.name = "Enima";
					this.weapon = new Weapon();
					weapon.create("Foam sword", 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
				
					s = new Spell("Minor heal", t.heal, 1, 1, 1, 1, 1);
					s.setDescription("/name feel better !");
					s.addHealProperty(1, 1, 1, 1);
					spells.Add(s);
				
					s = new Spell("Grenade", t.heal, 1, 5, 5, 5, 5);
					s.setDescription("/name make appear and throw a grenade on the ennemy");
					s.addDamages(6, 6, 6, 6);
					spells.Add(s);
				break;
			}
		}
		
		public int takeDamages (int damage, int type) {
			int res = 0;
			switch(type) {
				case FIRE : res = (damage * (this.fireresist / 100));
					this.pv = Mathf.Min(Mathf.Max(0, this.pv - res), this.pvmax);
				break;
				
				case WATER : res = this.pv - (damage * (this.waterresist / 100));
					this.pv = Mathf.Min(Mathf.Max(0, res), this.pvmax);
				break;
				
				case EARTH : res = (damage * (this.earthresist / 100));
					this.pv = Mathf.Min(Mathf.Max(0, this.pv - res), this.pvmax);
				break;
				
				case WIND : res = (damage * (this.windresist / 100));
					this.pv = Mathf.Min(Mathf.Max(0, this.pv - res), this.pvmax);
				break;
				
				default : res = damage;
					this.pv = Mathf.Min(Mathf.Max(0, this.pv - damage), this.pvmax);
				break;
			}
			
			return res;
		}
		
		public void heal(int heal, int type){
			takeDamages(-heal, type);
		}
		
		public bool isDead() {
			 return this.pv <= 0;
		}
		
		public int doDamages(int damage, int type){
		
			switch(type) {
				case FIRE : return (damage * (this.fireattack / 100));
				
				case WATER : return (damage * (this.waterattack / 100));
				
				case EARTH : return (damage * (this.earthattack / 100));
				
				case WIND : return (damage * (this.windattack / 100));
				
				default : return damage;
			}
		}
		
		public bool attack(Character c){
			 if(weapon.firecost > this.fire || weapon.watercost > this.water || weapon.earthcost > this.earth || weapon.windcost > this.wind) {
				return false;
			}
				
			this.fire -= weapon.firecost;
			this.water -= weapon.watercost;
			this.earth -= weapon.earthcost;
			this.wind -= weapon.windcost;
			
			System.Random ran = new System.Random();
			
			int fireDmg = ran.Next(weapon.fireattack, weapon.fireattackmax + 1);
			int waterDmg = ran.Next(weapon.waterattack, weapon.waterattackmax + 1);
			int earthDmg = ran.Next(weapon.earthattack, weapon.earthattackmax + 1);
			int windDmg = ran.Next(weapon.windattack, weapon.windattackmax + 1);
			
			if(weapon.fireattackmax > 0) {
				c.takeDamages(this.doDamages(fireDmg, FIRE), FIRE);
			}
			
			if(weapon.waterattackmax > 0) {
				c.takeDamages(this.doDamages(waterDmg, WATER), WATER);
			}
			
			if(weapon.earthattackmax > 0) {
				c.takeDamages(this.doDamages(earthDmg, EARTH), EARTH);
			}
			
			if(weapon.windattackmax > 0) {
				c.takeDamages(this.doDamages(windDmg, WIND), WIND);
			}
			
			return true;
		}
		
		public void collectMana(int collect, int type){
			switch(type) {
				case FIRE : this.fire = Mathf.Min(this.firemax, Mathf.Max(this.fire + collect, this.fire + collect + this.firecollect));
				break;
				
				case WATER : this.water = Mathf.Min(this.watermax, Mathf.Max(this.water + collect, this.water + collect + this.watercollect));
				break;
				
				case EARTH : this.earth = Mathf.Min(this.earthmax, Mathf.Max(this.earth + collect, this.earth + collect + this.earthcollect));
				break;
				
				case WIND : this.wind = Mathf.Min(this.windmax, Mathf.Max(this.wind + collect, this.wind + collect + this.windcollect));
				break;
				
				default : // Nothing to do here
				break;
			}
		}
		
		public bool canAttack() {
			return earth >= weapon.earthcost && fire >= weapon.firecost && wind >= weapon.windcost && water >= weapon.watercost;
		}
		
		public bool canCast(Spell s) {
			return earth >= s.earthcost && fire >= s.firecost && wind >= s.windcost && water >= s.watercost;
		}
		
		public bool shouldHeal() {
			return pv < pvmax / 4;
		}
		
	}
	
	
}