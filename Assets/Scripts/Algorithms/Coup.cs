using System;
using Algorithmics;
using RPG;
using UnityEngine;

namespace Algorithmics {
	public class Coup
	{
		public int orb1x;
		public int orb1y;
		public int orb2x;
		public int orb2y;
		
		public int g1,d1,h1,b1;
		
		public int g2,d2,h2,b2;
		public bool shuffle;
		public bool lancer_sort;
		public int changer_face;
		public bool attaque;
		public int valeur;
		public Spell spell;
		
		public Coup()
		{
		}
		
		/*
		 * Constructeur d'un coup
		 * @autor : Marie Gaignebet
		 * */
		public Coup(int d, int dd, int g, int gg, int h, int hh, int b, int bb)
		{	
			this.g1 = g;
			this.g2 = gg;
			this.d1 = d;
			this.d2 = dd;
			this.h1 = h;
			this.h2 = hh;
			this.b1 = b;
			this.b2 = bb;
		}
		
		/*
		 * Associe des orbs au coup
		 * @autor : Marie Gaignebet
		 * */			
		public void setCoup(int x, int y, int xx, int yy, int valeur)
		{
			this.orb1x = x;
			this.orb1y = y;
			this.orb2x = xx;
			this.orb2y = yy;
			this.valeur = valeur;
			this.shuffle = false;
			this.lancer_sort = false;
			this.attaque = false;
			this.changer_face = 0;
			this.spell = null;
			
		}
		
		/*
		 * Associe un shuffle au coup
		 * @autor : Marie Gaignebet
		 * */	
		public void setCoupShuffle(int valeur)
		{
			this.shuffle = true;
			this.lancer_sort = false;
			this.attaque = false;
			this.changer_face = 0;
			this.orb1x = -1;
			this.orb1y = -1;
			this.orb2x = -1;
			this.orb2y = -1;
			this.valeur = valeur;
			this.spell = null;
		}
		
		/*
		 * Associe un sort au coup
		 * @autor : Marie Gaignebet
		 * */	
		public void setCoupSort(int valeur, Spell s)
		{
			this.shuffle = false;
			this.lancer_sort = true;
			this.attaque = false;
			this.changer_face = 0;
			this.orb1x = -1;
			this.orb1y = -1;
			this.orb2x = -1;
			this.orb2y = -1;
			this.valeur = valeur;
			this.spell = s;
		}
		
		/*
		 * Associe un changement de face et son numéro au coup
		 * @autor : Marie Gaignebet
		 * */	
		public void setCoupFace(int valeur, int face)
		{
			this.shuffle = false;
			this.lancer_sort = false;
			this.attaque = false;
			this.changer_face = face;
			this.orb1x = -1;
			this.orb1y = -1;
			this.orb2x = -1;
			this.orb2y = -1;
			this.valeur = valeur;
			this.spell = null;
		}
		
		/*
		 * Associe une attaque au coup
		 * @autor : Marie Gaignebet
		 * */	
		public void setCoupAttaque(int valeur)
		{
			this.shuffle = false;
			this.lancer_sort = false;
			this.attaque = true;
			this.changer_face = 0;
			this.orb1x = -1;
			this.orb1y = -1;
			this.orb2x = -1;
			this.orb2y = -1;
			this.valeur = valeur;
			this.spell = null;
		}
		
		//getteurs et setteurs 
		
		public int getX1()
		{
			return orb1x;
		}
		
		public int getY1()
		{
			return orb1y;
		} 
		
		public int getX2()
		{
			return orb2x;
		} 
		
		public int getY2()
		{
			return orb2y;
		}
		
		public int getValeur()
		{
			return valeur;
		}
	}
}
	
	
