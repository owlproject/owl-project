using System;

namespace Algorithmics {
	public class Orb {
		
		public const int FIRE = 1;
		public const int WATER = 2;
		public const int EARTH = 3;
		public const int WIND = 4;
		public const int XP = 5;
		public const int GOLD = 6;
		
		public int type;
		public bool aDetruire;
		
		public Orb (int type) {
			this.type = type;
		}
	}
}

