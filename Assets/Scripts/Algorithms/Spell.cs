using System;
using System.Collections;
using RPG;
using UnityEngine;

namespace RPG {
	
	public class Spell {
		
		public string name = "";
		public string description = "";
		public Texture icon = null;
		public int level = 1;
		
		public bool attack = false;
		
		public int fireattack = 0;
		public int waterattack = 0;
		public int earthattack = 0;
		public int windattack = 0;
		
		public bool heal = false;
		
		public int fireheal = 0;
		public int waterheal = 0;
		public int earthheal = 0;
		public int windheal = 0;
		
		public ArrayList transformField = null;
		
		public int firecost = 0;
		public int watercost = 0;
		public int earthcost = 0;
		public int windcost = 0;
		
		public Spell (string name, Texture icon, int level, int firecost, int watercost, int earthcost, int windcost) {
			this.name = name;
			this.level = level;
			this.icon = icon;
			
			this.firecost = firecost;
			this.watercost = watercost;
			this.earthcost = earthcost;
			this.windcost = windcost;
		}
		
		public void setDescription(string description) {
			this.description = description;	
		}
		
		public void addDamages(int fireattack, int waterattack, int earthattack, int windattack) {
			this.attack = true;
			this.fireattack = fireattack;
			this.waterattack = waterattack;
			this.earthattack = earthattack;
			this.windattack = windattack;
		}
		
		public void addHealProperty(int fireheal, int waterheal, int earthheal, int windheal) {
			heal = true;
			this.fireheal = fireheal;
			this.waterheal = waterheal;
			this.earthheal = earthheal;
			this.windheal = windheal;
		}
		
		public void addTransformFieldProperty(ArrayList list) {
			this.transformField = list;
		}
		
		public string replace(string playerName) {
			return description.Replace("/name", playerName);
		}
		
		public bool launch(Character player, Character enemy) {
			
			if(player.fire < firecost || player.water < watercost || player.earth < earthcost || player.wind < windcost) return false;
			else {
				player.fire -= firecost;
				player.water -= watercost;
				player.earth -= earthcost;
				player.wind -= windcost;
			}
			
			if(attack) {
				if(fireattack > 0) {
					enemy.takeDamages(player.doDamages(fireattack, Character.FIRE), Character.FIRE);
				}
				
				if(waterattack > 0) {
					enemy.takeDamages(player.doDamages(waterattack, Character.WATER), Character.WATER);
				}
				
				if(earthattack > 0) {
					enemy.takeDamages(player.doDamages(earthattack, Character.EARTH), Character.EARTH);
				}
				
				if(windattack > 0) {
					enemy.takeDamages(player.doDamages(windattack, Character.WIND), Character.WIND);
				}
				
				GameObject.Find("Enemy").GetComponent<Enemy>().majBar();
			}
			
			if(heal) {
				if(fireheal > 0) player.heal(fireheal, Character.FIRE);
				if(waterheal > 0) player.heal(waterheal, Character.WATER);
				if(windheal > 0) player.heal(windheal, Character.WIND);
				if(earthheal > 0) player.heal(earthheal, Character.EARTH);
			}
			
			return true;
		}
		
		public Color manaColor() {
			Color c = new Color(0, 0, 0, 0);
			bool oneColorFound = false;
			
			if(fireattack > 0) {
				oneColorFound = true;
				c = Color.red;
			}
			
			if(waterattack > 0) {
				if(oneColorFound) {
					c = Color.white;	
				} else {
					oneColorFound = true;
					c = Color.blue;	
				}
			}
			
			if(earthattack > 0) {
				if(oneColorFound) {
					c = Color.white;	
				} else {
					oneColorFound = true;
					c = new Color(104, 69, 18, 1);	
				}
			}
			if(windattack > 0) {
				if(oneColorFound) {
					c = Color.white;	
				} else {
					oneColorFound = true;
					c = Color.yellow;	
				}
			}
			
			return c;
		}
	}
}

