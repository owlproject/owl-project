using System;

namespace Algorithmics
{
	public class Crush
	{
		public int x, y, oldType, newType;
		
		public Crush (int x, int y, int oldType, int newType)
		{
			this.x = x;
			this.y = y;
			this.oldType = oldType;
			this.newType = newType;
		}
	}
}

