using System;
using System.Collections;
using System.IO;
using Interface;
using OWL_Project;
using UnityEngine;
using RPG;

namespace Algorithmics {
	
	public class Cube {
		
		private const int RIGHT = 1;
		private const int BOTTOM = 2;
		private Orb[,] cube;
		public int length;
		public OWL_Project.Cube MMCube;
		
		public bool ingame = false;
		
		
		public Cube (int dimension) {
			cube = new Orb[dimension,dimension];
			length = dimension;
			
			/*Version steven
			 * System.Random ran = new System.Random();
			for(int i = 0; i < dimension; i++) {
				for (int j = 0; j < dimension; j++) {
					cube[i,j] = new Orb(ran.Next(1, 7));
				}
				
			while(playCombi().Count > 0);
			}*/
			
			//version NORMALE
			MMCube = new OWL_Project.Cube();
			MMCube.init(dimension);
			
			int faceCourante = MMCube.getFaceCourante();
			
			Plateau pla = MMCube.getPlateau(faceCourante);
			for(int i = 0; i < dimension; i++) {
				for (int j = 0; j < dimension; j++) {
					int type = pla.getOrb(i,j).getType();
					cube[i,j] = new Orb(type);
				}
			}
			
			while(playCombi().Count > 0);
			updateMMCube();
			ingame = true;
					
		}
		
		public bool permutation(int x1, int y1, int x2, int y2) {
			if(Math.Abs(x1 - x2) + Math.Abs(y1 - y2) > 1) return false;
			int tmp = cube[x1, y1].type;
			cube[x1, y1].type = cube[x2, y2].type;
			cube[x2, y2].type = tmp;
			
			bool res = nbCombinaisons() > 0;
			
			if(!res) {
				cube[x2, y2].type = cube[x1, y1].type;
				cube[x1, y1].type = tmp;
			}
			
//			Debug.Log("("+x1+","+y1+") : "+cube[x1,y1].type+" // ("+x2+","+y2+"): "+cube[x2,y2].type);
//			Debug.Log("("+x1+","+y1+") : "+MMCube.getPlateau(MMCube.getFaceCourante()).getOrb(x1,y1).getType()+" // ("+x2+","+y2+"): "+MMCube.getPlateau(MMCube.getFaceCourante()).getOrb(x2,y2).getType());
			updateMMCube();
//			Debug.Log("("+x1+","+y1+") : "+cube[x1,y1].type+" // ("+x2+","+y2+"): "+cube[x2,y2].type);
//			Debug.Log("("+x1+","+y1+") : "+MMCube.getPlateau(MMCube.getFaceCourante()).getOrb(x1,y1).getType()+" // ("+x2+","+y2+"): "+MMCube.getPlateau(MMCube.getFaceCourante()).getOrb(x2,y2).getType());
			
			return res;
		}
		
		public int nbCombinaisons() {
			
			int combi = 0;
			
			for(int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					if(combinaison(i, j, false) >= 3) combi++;
				}
			}
			
			return combi;
		}
		
		public void play(Character c, bool swiched, int x1, int y1, int x2, int y2) {
			ArrayList orbs = new ArrayList();
			int size; 
			do {
				size = orbs.Count;
				ArrayList orbsSup = playCombi();
				
//				Debug.Log ("PlayCombi a trouvé "+orbsSup.Count+" orbes à détruire !");
				
				if(orbsSup.Count > 0) {
					
					foreach(object o in orbsSup) {
						orbs.Add(o);
						Crush crush = (Crush) o;
						switch(crush.oldType) {
							case Orb.EARTH : c.collectMana(1, Character.EARTH);
								break;
							case Orb.FIRE : c.collectMana(1, Character.FIRE);
								break;
							case Orb.WATER : c.collectMana(1, Character.WATER);
								break;
							case Orb.WIND : c.collectMana(1, Character.WIND);
								break;
							case Orb.GOLD  : c.gold += 1;
								break;
							case Orb.XP : c.xp += 1;
								break;
						}
					}
				}
				
			} while(orbs.Count > size);
			
			GenerateOrbs go = GameObject.Find("OrbGrid").GetComponent<GenerateOrbs>();
			
			if(swiched) {
				
				// On récupère notre instance du script GenerateOrbs qui est attaché au GameObject "OrbGrid"
				
				go.firstSwitch = (GameObject) go.orbs[x1 * length + y1];
				go.secondSwitch = (GameObject) go.orbs[x2 * length + y2];
				
				// Do we move horizontally or vertically ?
				if(Mathf.Abs(y2 - y1) > Mathf.Abs(x2 - x1)) {
					// Do we move left or right ?
					if(y1 - y2 > 0) {
						go.switchOrbsFromExt(GenerateOrbs.LEFT, orbs);
					} else {
						go.switchOrbsFromExt(GenerateOrbs.RIGHT, orbs);
					}
				} else {
					// Do we move up or down ?
					if(x1 - x2 > 0) {
						go.switchOrbsFromExt(GenerateOrbs.TOP, orbs);
					} else {
						go.switchOrbsFromExt(GenerateOrbs.BOTTOM, orbs);
					}
				}
			} else {
				go.destroyOrbsFromExt(orbs);
			}
			
			updateMMCube();	
		}
		
		public ArrayList playCombi() {
//			if(ingame) Debug.Log("ingame = true;");
//			else Debug.Log("ingame = false;");
			if(ingame) 
				return playCombiIngame();
			
			ArrayList res = new ArrayList();
			
			for(int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					combinaison(i, j, true);
				}
			}
		
			System.Random ran = new System.Random();
			System.Random ran2 = new System.Random();
			
			int lastran = -1;
			int lastlastran = -1;
			
			for(int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					if(cube[i, j].aDetruire) {
						
						int newOrb = -1;
						int oldType = cube[i, j].type;
						cube[i, j].aDetruire = false;
						
						int ranNum = ran.Next(1, 7);
						while(ranNum == lastran && ranNum == lastlastran) {
							ranNum = ran2.Next(1, 7);
						}
						lastlastran = lastran;
						lastran = ranNum;
						newOrb = ranNum;
					
						cube[i,j].type = newOrb;
						res.Add(new Crush(i, j, oldType, cube[i, j].type));
					}
				}
			}
			
			return res;
			
		}
		
		public ArrayList playCombiIngame(){
//			Debug.Log("deb playCombiIngame");
			
			ArrayList res = new ArrayList();
			
			for(int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					combinaison(i, j, true);
				}
			}
			
			for(int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					if(cube[i, j].aDetruire) {
						
						int newOrb = -1;
						int oldType = cube[i, j].type;
						cube[i, j].aDetruire = false;
						
						OWL_Project.Orb ancien_orb = MMCube.getPlateau(MMCube.getFaceCourante()).getOrb(i,j);
						ancien_orb.detruit();
						OWL_Project.Orb nouveau_orb = MMCube.randomOrb(ancien_orb.getX(), ancien_orb.getY(), ancien_orb.getZ());
							//MMCube.rechercheOrb(ancien_orb.getX(), ancien_orb.getY(), ancien_orb.getZ(), MMCube.getFaceCourante());
						newOrb = nouveau_orb.getType();
												
						cube[i,j].type = newOrb;
						res.Add(new Crush(i, j, oldType, cube[i, j].type));
					}
				}
			}
			
			return res;
		}
		
		public int combinaison(int x, int y, bool ingame) {
			int bottom = 1;
			int right = 1;
			int type = cube[x, y].type;
			
			int res = 0;
			
			int i = x + 1;
			while(i < length && type == cube[i, y].type) {
				bottom++;
				i++;
			}
			
			i = y + 1;
			while(i < length && type == cube[x, i].type) {
				right++;
				i++;
			}
			
			if(bottom >= 3) {
				if(ingame) cube[x, y].aDetruire = true;
				
				i = x + 1;
				while(i < length && type == cube[i, y].type) {
					if(ingame) cube[i, y].aDetruire = true;
					i++;
				}
				
				res += bottom;
			}
			
			if(right >= 3) {
				if(ingame) cube[x, y].aDetruire = true;
				
				i = y + 1;
				while(i < length && type == cube[x, i].type) {
					if(ingame) cube[x, i].aDetruire = true;
					i++;
				}
				
				res += right;
			}
			
			return res;
		}
		
		public void majCube(int face) {
			MMCube.getFace(face);
			Plateau pla = MMCube.getPlateau(face);
			for(int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					int type = pla.getOrb(i,j).getType();
					cube[i,j] = new Orb(type);
				}
			}
		}
		
		public void printCube(string path) {
			
			string res = "\n\n";
			for(int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					res += cube[i, j].type+" ";
				}
				res += "\n";
			}
			
			using (StreamWriter sw = File.AppendText(path)) {
	            sw.Write(res);
	        }
		}
		
		public void printMsg(string path, string msg) {
			
			string res = "\n\n"+msg;
			
			using (StreamWriter sw = File.AppendText(path)) {
	            sw.Write(res);
	        }
		}
		
		public int getOrb(int x, int y) {
			return cube[x, y].type;	
		}
		
		public int getTaille() {
			return length;	
		}
		
		public void setType(int x, int y, int type)
		{
			this.cube[x,y].type = type;
		}
		
		public void updateMMCube()
		{
			for(int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					OWL_Project.Orb o = MMCube.getPlateau(MMCube.getFaceCourante()).getOrb(i,j);
					
					int type = cube[i,j].type;
					o.setType(type);					
				}
			}
		}
	}
}

