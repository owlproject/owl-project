/* IMPORTANT A LIRE AVANT MODIFICATION
 
 	Pour ouvrir le projet contenant ces sources, utiliser un éditeur comme monodevelop puis faites "ouvrir" --> OWL_Project.csproj
 	Cela permet de récupérer correctement les fichiers et leurs namespaces respectifs, et exécuter beaucoup plus simplement le code
 	
 	Marc
  
*/

using System;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace OWL_Project
{
	public class Cube
	{
		private Orb[,,] matrice; // on recuperera les orb avec des divisons, modulo ou autres (coordonnées)
		private int taille;
		private Plateau[] plateaux;
		private int faceCourante;
		private readonly int nombrePlateaux = 6;	
		private int combinaisonsDetectees;
		private long getFace_ms;
		private long retrouveFace_ms;
		private long detecteCombinaison_ms;
		

		/*		 Objet random utilisé pour générer des orbes aléatoires
		 * Nécessaire car il faut utiliser le meme objet random
		 * pour ne pas générer une séquence de nombre identique pendant la génération du cube
		 * */
		private System.Random randomNumber = new System.Random();

		/*		*************************************************
		 * 					INITIALISATIONS
		 **************************************************
		*/
		/*
		* Nouvelle instance de Cube
		* @author Marie Gaignebet
		* @modified Marc Papillon
		* @last Marie Gaignebet
		* */
		public Cube()
		{
			// matrice --> initialiser avec la taille
			taille = 0;
			plateaux = new Plateau[nombrePlateaux];
			faceCourante = 1;
			
			combinaisonsDetectees = 0;
			getFace_ms = 0;
			detecteCombinaison_ms = 0;
		}

		/*		*
		 * Initialise le Cube
		 * @author Marie Gaignebet
		 * @modified Marc Papillon
		 * @last Marie Gaignebet
		 * */
		public void init(int p_taille)
		{
			if(p_taille%2 != 0) 
				taille = p_taille;
			else
			{
				Console.WriteLine("Erreur : le cube doit avoir une taille impaire.");
				Environment.Exit(0); //erreur fatale
			}

			initMatrice(); 
			genereMatrice();
		}

		/*		*
		 * Initialise la matrice
		 * @author Amine Salmi 
		 * @modified Marie Gaignebet 
		 * */
		public void initMatrice()
		{
			matrice = new Orb[taille, taille, taille];

			for (int x = 0; x < taille; x++)
			{
				for (int y = 0; y < taille; y++)
				{
					for (int z = 0; z < taille; z++)
					{
						setOrb(x, y, z, new Orb(x, y, z)); //type init --> a generer aleatoirement
					}
				}
			}
		}

		/*		
		 * Create the random orbs
		 * @author Amine Salmi
		 * @modified Marie Gaignebet, Marc Papillon
		 * @last Marie Gaignebet
		 * */
		public void genereMatrice()
		{	
			Stopwatch watch = new Stopwatch();
			
			List<Orb> orbResult = new List<Orb>();
			int combinaisonDetectee = 0;
			for(int x = 0; x < taille; x++)
			{
				for(int y = 0; y < taille; y++)
				{
					for(int z = 0; z < taille; z++)
					{					
						setOrb(x, y, z, randomOrb(x, y, z));
					}
				}
			}			
			
			watch.Start();
			for(int face = 1; face <= nombrePlateaux; face++)
			{		
				plateaux[face-1] = new Plateau(taille, face);
				getFace(face);
			}
			watch.Stop();
			this.getFace_ms += watch.ElapsedMilliseconds;
			watch.Reset();
			
			//stabilise();
			
		}

		/*		*************************************************
		 * 					GET et SET
		 **************************************************
		*/

		public int getCombinaisonsDetectees()
		{
			return combinaisonsDetectees;	
		}

		/*		*
		 * Get specific orb from cube
		 * @author Amine Salmi  
		 * @Marie : quelle différence avec getOrb(int x, int y, in z)?
		 * */
		public Orb getOrb(int x, int y, int z)
		{
			return matrice[x, y, z];
		}

		/*		*
		 * Modify specific orb from cube and its equivalent on the "Plateau"
		 * @author Amine Salmi
		 * */
		public void setOrb(int x, int y, int z, Orb o)
		{
			matrice[x, y, z] = o;
			if(y == 0 || y == taille)
			{
				//TODO : SetOrb on Plateau
			}
		}

		/*		*
         * Get current face of a cube
         * @author Marie Gaignebet
         **/
		public int getFaceCourante()
		{
			return this.faceCourante;
		}

		/*		 Set current face of a cube
         * @author Marie Gaignebet
         **/
		public void setFaceCourante(int face)
		{
			this.faceCourante = face;
		}

		/*		*
         * Get face of a cube
         * @author Amine Salmi
         **/
		public Plateau getPlateau(int face)
		{
			return plateaux[face -1];
		}

		/*		*
		 * Recupere une face du cube pour l'initialisation (a ameliorer pour le reste de la partie: detection de la couche superieure)
		 * @author Marie Gaignebet
		 * @modified Marc Papillon
		 * @ param: numero de la face
		 * @return plateau correspondant
		 * */
		public void getFace(int face)
		{
			/*			*
			 * Remarques sur la recuperation des faces
			 * 
			 * face 1:: face: matrice = tab[,,0] 
			 * face 2:: cote droit: matrice = tab[,taille,]
			 * face 3:: derriere: matrice = tab[,,taille]
			 * face 4:: cote gauche: matrice = tab[,0,]
			 * face 5:: dessus: matrice = tab[0,,]
			 * face 6:: dessous: matrice = tab[taille,,]		
			 * */

			int x;
			int y;
			int z;			

			Plateau newMatrice = getPlateau(face);
			if ( face == 5 || face == 6)
			{	
				if ( face == 5) 
					y = 0; 
				else
					y = taille-1;

				for( x = 0; x < taille; x ++) 
					for( z = 0; z < taille; z ++) 
						newMatrice.setOrb(x, z, rechercheOrb(x, y, z, face));
			}
			
			else if ( face == 2 || face == 4)
			{	
				if ( face == 2) 
					x = taille - 1; 
				else
					x = 0;

				for( y = 0; y < taille; y ++) 
					for( z = 0; z < taille; z ++) 
					{
						Orb found = rechercheOrb(x, y, z, face);
						found.displayCoord();
						newMatrice.setOrb(y, z, rechercheOrb(x, y, z, face));
					}
			}
			
			else //if ( face == 3 || face == 1)
			{	
				if ( face == 1) 
					z = 0; 
				else
					z = taille - 1;
				
				for( x = 0; x < taille; x ++)
					for( y = 0; y < taille; y ++) 
					{
						Orb found = rechercheOrb(x, y, z, face);
						found.displayCoord();
						newMatrice.setOrb(x, y, rechercheOrb(x, y, z, face));
					}
			}
		}
		
		public int getTaille()
		{
			return this.taille;	
		}
		/*		*************************************************
		 * 					CONTROLES
		 **************************************************
		*/

		/**************************************************
		* 					JEU
		**************************************************
		*/

		/** 
		* Fait tourner le cube pour actualiser le plateau actif
		* @author Marie Gaignebet
		* */
		public void tourner()//TODO
		{

		}

		/*		*
		 * detecte une combinaison
		 * @author Marie Gaignebet
		 * @modified Marc Papillon
		 * */
		public List<Orb> detecteCombinaison(int x, int y, int face)
		{
			Orb orbTest = getPlateau(face).getOrb(x, y);
			List<Orb> result = new List<Orb>();
			List<Orb> resultIntermediaire = new List<Orb>();

			int a, b;
			for(int i = 0; i <= 3; i++)
			{
				a = -2*(i%2) + 1;
				b = i / 2;

				resultIntermediaire = detecteCompatibilite(x, y, face, a, b, result);

				foreach (Orb o in resultIntermediaire)
				{
					if (!result.Contains(o))
						result.Add(o);
				}
			}

			return result;
		}


		/*		*
		 * Teste la compatibilite pour les combinaisons
		 * @author Marie Gaignebet
		 * @param int x abscisse
		 * @param int y ordonnee
		 * @param int face face du cube
		 * @param int variable direction de la variation dans la matrice du plateau
		 * @param int dimension = 0 variation sur x, sur y sinon 
		 * @param List<Orb> result liste des orbs retenus
		 * @param Orb reference point de depart
		 * 
		 * */
		public List<Orb> detecteCompatibilite(int x, int y, int face, int variable, int dimension, List<Orb> result)
		{
			int xx = x;
			int yy = y;
			int type = getPlateau(face).getOrb(x, y).getType();
			int typeTrouve = type;
			List<Orb> orbsCompatibles = new List<Orb>();

			do
			{
				orbsCompatibles.Add(getPlateau(face).getOrb(xx, yy));

				if(xx > 0 && yy > 0 && xx < taille - 1 && yy < taille - 1)
				{
					if( dimension == 0 ) xx += variable;
					else yy += variable;
					typeTrouve = getPlateau(face).getOrb(xx, yy).getType();
				}
				else
					type = -2;

			} while ( typeTrouve == type);

			if( orbsCompatibles.Count < 3)
				orbsCompatibles.Clear();				

			return orbsCompatibles;
		}


		/**
         * Swap two orbs
         * @author Amine Salmi
         * */
		public void permute(Orb o1, Orb o2)
		{
			int xo1, yo1, zo1, xo2, yo2, zo2;
			xo1 = o1.getX ();
			yo1 = o1.getY ();
			zo1 = o1.getZ ();
			xo2 = o2.getX ();
			yo2 = o2.getY ();
			zo2 = o2.getZ ();
			setOrb(xo1, yo1, zo1, o2);
			setOrb(xo2, yo2, zo2, o1);
			o1.setX (xo2);
			o1.setY (yo2);
			o1.setZ (zo2);
			o2.setX (xo1);
			o2.setY (yo1);
			o2.setZ (zo1);
			
			xo1 = o1.getX ();
			yo1 = o1.getY ();
			zo1 = o1.getZ ();
			xo2 = o2.getX ();
			yo2 = o2.getY ();
			zo2 = o2.getZ ();
		}

		public Tuple<int, int, int> detruit(Orb o, int face)
		{
			Orb newOrb = rechercheOrb (o.getX(), o.getY(), o.getZ(), face);
			o.detruit ();
			Tuple<int, int, int> coordOrb = coordDansPlateau (newOrb, face);
			getPlateau(faceCourante).setOrb (coordOrb.First, coordOrb.Second, newOrb);
			return coordOrb;
		}

		/**
         * Swap two orbs and destroys the occurences recursively.Prend une liste d'orb à détruire,, les détruit et se relance si la destruction provoque d'autre combinaisons
         * @author Amine Salmi
         * */
		public void permute_detruit_rec(List<Tuple<int, int, int>> destructibles, int face)
		{
			List<Tuple<int, int, int>> dest_rec = new List<Tuple<int, int, int>> ();
			bool continuer_destruction = false;
			foreach (Tuple<int, int, int> t in destructibles) 
			{
				int x1 = t.First;
				int y1 = t.Second;
				int fface = t.Tierce;
				List<Orb> comb = detecteCombinaison (x1, y1, fface);
				if (comb.Count > 0)
				{
					continuer_destruction = true;
					foreach(Orb o in comb)
					{
						dest_rec.Add(detruit(o, face));
					}
				}
			}
			if (continuer_destruction) 
			{
				permute_detruit_rec (dest_rec, face);
			} 
		}

		/**
		 * Swap two orbs and do the necessary checks for the "plateau".
		 * @author Amine Salmi
		 */ 
		public void permute_plateau(Orb o1, Orb o2)
		{
			//Calcul pour voir si ça impacte une autre face que la face initiale: sachant que pour le moment juste O1 est sur le bord
			List<int> faces_1 = retrouveFace(o1.getX(), o1.getY(), o1.getZ ());
			List<int> faces_2 = retrouveFace(o2.getX(), o2.getY(), o2.getZ ());
			foreach( int f in faces_1)
			{
				getFace(f);
			}
			foreach( int f in faces_2)
			{
				getFace(f);
			}
		}

		/*		
		 * Destroys the orbs that must be destroyed after a swap.: lance la destruction les orbs ap permutation si elle est possble, sinon restitue l'état initial
		 * @author Amine Salmi
		 * Ajouter un paramètre de sortie (List<Orb>)
		 * */
		public void permute_detruit(int f, int x1, int y1, int x2, int y2)
		{
			Orb o1, o2;
			o1 = getPlateau(faceCourante).getOrb(x1, y1);
			o2 = getPlateau(faceCourante).getOrb(x2, y2);
			int xo1, yo1, zo1, xo2, yo2, zo2;
			xo1 = o1.getX ();
			yo1 = o1.getY ();
			zo1 = o1.getZ ();
			xo2 = o2.getX ();
			yo2 = o2.getY ();
			zo2 = o2.getZ ();
			this.permute (o1, o2);
			permute_plateau (o1, o2);
			//We test if such a combination exists
			var combinaisons1 = detecteCombinaison (x1, y1, f);
			var combinaisons2 = detecteCombinaison (x2, y2, f);
			List<Tuple<int, int, int>> detruits = new List<Tuple<int, int, int>>();
			if (combinaisons1.Count == 0 && combinaisons2.Count == 0)
			{
				this.permute (o1, o2);
				permute_plateau (o1, o2);
			}
			else 
			{
				//TODO : Destruction and replacement of Orb in both Plateau and Cube
				Tuple<int, int, int> t1 = Tuple.Create (x1, y1, f);
				Tuple<int, int, int> t2 = Tuple.Create (x2, y2, f);
				detruits.Add (t1);
				detruits.Add (t2);
				permute_detruit_rec (detruits, f);
			}
		} 

		/*		* 
		 * Comble les vides lors de l'atteinte de l'orb central
		 * Peut etre ameliore par recherche plus intelligente a partir du plateau actif  : fait dans rechercheOrb en partie
		 * On aurait alors un chemin vers le detruit
		 * @author Marie Gaignebet
		 * */
		public void regeneration()
		{
			for(int x = 0; x < taille; x++)
			{   for(int y = 0; y < taille; y++)
				{   for(int z = 0; z < taille; z++)
					{  
						if( getOrb(x, y, z).getType() == -2) setOrb(x, y, z, randomOrb(x, y, z));
					}
				}
			}
		}

		/*		*************************************************
		 * 					RECHERCHE (= get speciaux)
		 **************************************************
		*/

		/*		*
		* retrouve les coordonnees d'un orb d'un plateau a partir de ces coordonnees dans le cube
		* @author Marie Gaignebet
		* @result :: liste de tuples < int x, int y, int face>
		* */
		public Tuple<int, int, int> coordDansPlateau(Orb orb, int face)
		{
			int x = orb.getX ();
			int y = orb.getY ();
			int z = orb.getZ ();
			/*						*
			 * Remarques sur la recuperation des faces
			 * 
			 * face 1:: face: matrice = tab[,,0]
			 * 
			 * face 2:: cote droit: matrice = tab[,taille,]
			 * 
			 * face 3:: derriere: matrice = tab[,,taille]
			 * 
			 * face 4:: cote gauche: matrice = tab[,0,]
			 * 
			 * face 5:: dessus: matrice = tab[0,,]
			 * 
			 * face 6:: dessous: matrice = tab[taille,,]		
			 * */
			Tuple<int, int, int> t = null;
			if( face == 1 || face == 3) 		t = Tuple.Create (x, y, face);
			
			else if( face == 2  || face == 4)	t = Tuple.Create (x, z, face);
			
			else if( face == 5 || face == 6) 	t = Tuple.Create (y, z, face);
			return t;			
		}

		/**
		* Recherche d'une orbe du cube
		* @author Marie Gaignebet
		* @param int x: absisse de l'orb dans la matrice du cube 
		* @param int y: ordonnee de l'orb dans la matrice du cube 
		* @param int z: profondeur de l'orb dans la matrice du cube 
		* @param int face: recherche en profondeur = 0, simple get = 1
		* */

		public Orb rechercheOrb(int x, int y, int z, int face)
		{			
			switch(face) {
				case 1 : z += 1;
				break;
				
				case 2 : x -= 1;
				break;
				
				case 3 : z -= 1;
				break;
				
				case 4 : x += 1;
				break;
				
				case 5 : y += 1;
				break;
				
				case 6 : y -= 1;
				break;
				
			}		
			
			Orb orb = new Orb(x, y, z);
			
			if (!(x > taille || x < 0 || y > taille || y < 0 || z > taille || z < 0))
				orb = getOrb(x, y ,z);
			
			return orb;
		}

		/*		*
		 * retrouve la ou les faces ou est situe un orb 
		 * @author Marie Gaignebet
		 * @result ::null si au centre, sinon pour chaque objet dans l'arraylist: result[0] = x, result[1] = y, result[2] = numero de la face,
		 * */
		public List<int> retrouveFace(int x, int y, int z)
		{

			/**
			 * Remarques sur la recuperation des faces
			 * 
			 * face 1:: face: matrice = tab[,,0]
			 * 
			 * face 2:: cote droit: matrice = tab[,taille,]
			 * 
			 * face 3:: derriere: matrice = tab[,,taille]
			 * 
			 * face 4:: cote gauche: matrice = tab[,0,]
			 * 
			 * face 5:: dessus: matrice = tab[0,,]
			 * 
			 * face 6:: dessous: matrice = tab[taille,,]		
			 * */			
			
			List <int> result = new List<int>();
			Orb o = getOrb(x, y, z);
			
			for(int f = 1; f < 7; f ++)
				if( isSurFace(o, f)) result.Add(f);
			return result;
		}
		
		/**
		* Renvoie vrai si l'orb appartient à la face entrée en parametre, faux sinon
		* @author Amine Salmi
		* */
		public bool isSurFace(Orb o, int face)
		{
			
			int test = -1;
			int depX = 0;
			int depY = 0;
			int depZ = 0;
			int dir = 0;
			
			if( face == 1 || face == 3)
			{
				test = o.getZ();
				depZ = 1;
			}
			else if( face == 4 || face == 2)
			{
				test = o.getX();
				depX = 1;
			}
			else if( face == 5 || face == 6)
			{
				test = o.getY();
				depY = 1;
			}
			
			if( face == 1 || face == 4 || face == 5)
				dir = -1;
			else if( face == 2 || face == 3 || face == 6)
				dir = 1;
			
			bool bel = true;
			while ((test > 0 && dir == -1) || ( test < taille -1 && dir == 1))
			{
				test += dir;
				bel = bel && getOrb(o.getX() + (depX * dir), o.getY() + (depY * dir), o.getZ() + (depZ * dir)).estDetruit();
			}
			return bel;		
			
		}
		

		/**************************************************
		 * 					DIVERS
		 **************************************************
		*/

		/**
		* Génère un orb aléatoirement parmi une liste de types (liste vide = pas de contraintes)
		* amelioration a apporter: generer en fonction d'un contexte (monde, joueur, adversaire...) 
		* @author Marie Gaignebet
		* @modified Marc Papillon
		* */
		public Orb randomOrb(int x, int y, int z)
		{
			int numero = randomNumber.Next(0, 6);

			switch (numero)
			{
				case 0: return new OrbAir(x, y, z);
				case 1: return new OrbEau(x, y, z);
				case 2: return new OrbFeu(x, y, z);
				case 3: return new OrbOr(x, y, z);
				case 4: return new OrbTerre(x, y, z);
				case 5: return new OrbXp(x, y, z);
	
				default: return null;
			}
		}

		/*		*
         * Génère un orb aléatoirement parmi une liste de types
         * amelioration a apporter: generer en fonction d'un contexte (monde, joueur, adversaire...) 
         * @author Marie Gaignebet
         * */
		public Orb randomOrb(int x, int y, int z, List<int> values)
		{
			if (values.Count == 0)
			{
				Console.WriteLine("Erreur randomOrb : Liste vide");
				System.Environment.Exit(-1);
			}

			int max = 6;
			int numero = 0;

			max = values.Count; 
			numero = values[randomNumber.Next(0, max)];

			switch (numero)
			{
				case 1: return new OrbFeu(x, y, z);
				case 2: return new OrbEau(x, y, z);
				case 3: return new OrbTerre(x, y, z);
				case 4: return new OrbAir(x, y, z);
				case 5: return new OrbOr(x, y, z);
				case 6: return new OrbXp(x, y, z);
				default: Console.WriteLine("Erreur randomOrb : numero non compris dans le switch"); return null;
			}
		}

		/*		*************************************************
		 * 					DISPLAY
		 **************************************************
		*/

		/** 
		* Affichage en console du cube
		* @author Amine Salmi
		* @modified Marc Papillon
		* @last Marie Gaignebet --> display devient displayCouches
		* */
		public void displayCouches()
		{
			for(int x = 0; x < taille; x++)
			{
				Console.WriteLine("Couche " + (x+1) + " :");
				Console.WriteLine();

				for(int y = 0; y < taille; y++)
				{   for(int z = 0; z < taille; z++)
					{
						Console.Write("|" + " " + getOrb(x, y, z).TypeToString()[0] + " ");
					}
					Console.WriteLine();
				}
				Console.WriteLine();
			}
		}

		/*		* 
		 * Affichage en console des plateaux
		 * @author Marie Gaignebet
		 * */
		public void displayPlateaux()
		{
			for(int i = 0; i < this.nombrePlateaux; i++)
			{
				Console.WriteLine("Plateau : " + i);
				plateaux[i].display();
				Console.WriteLine();
			}
		}
		
		public bool stabilise()
		{
			return false;
			//remplacer par la solution suivante: tout générer -> si combi détectée: regarder voisins et générer un orb qui n'est pas dans liste en remplaçant petit à petit
			/*for(int face = 1; face <= nombrePlateaux; face++)
			{		
				for(int x = 0; x < taille; x++)
				{
					for(int y = 0; y < taille; y++)
					{
						List<int> list_valid_values = new List<int>() {1, 2, 3, 4, 5, 6};
						int refaire = 0;
						do{
							combinaisonDetectee = 0;

							int xx, yy, zz;
							xx = getPlateau(face).getOrb(x,y).getX();
							yy = getPlateau(face).getOrb(x,y).getY();
							zz = getPlateau(face).getOrb(x,y).getZ();
							
							watch.Start();
							List<int> result = retrouveFace(xx, yy, zz);
							watch.Stop();
							this.retrouveFace_ms += watch.ElapsedMilliseconds;
							watch.Reset();
							
							watch.Start();
							foreach (int fface in result)
							{
								orbResult = detecteCombinaison(x, y, fface);
								combinaisonDetectee = combinaisonDetectee + orbResult.Count;
							}
							watch.Stop();
							this.detecteCombinaison_ms += watch.ElapsedMilliseconds;
							watch.Reset();

							if (combinaisonDetectee > 0)
							{
								int type = getOrb (xx, yy, zz).getType();

								list_valid_values.Remove(type);
								if( list_valid_values.Count == 0)
								{
									Console.WriteLine("Il n'y a plus de possibilités - On refait le cube");								

									refaire = 1;
									break;
									//Environment.Exit(-1);
								}

								Orb randOrb = randomOrb(xx, yy, zz, list_valid_values);
								setOrb(xx, yy, zz, randOrb);

								combinaisonsDetectees++; // Compte le nombre de combinaisons détectées durant la génération
							}

							orbResult.Clear();

						} while( combinaisonDetectee != 0 && refaire != 1) ;
						
						//Ajouter partie pour revenir en arriere dans le cube si on a teste toutes les possibilites
//						if(refaire == 1) 
//						{
////							genereMatrice();
////							break;
//							if(y == 0)
//							{
//								x -= 1;
//								y = taille-2;
//							}
//							else
//								y -= 2;
//						}			
					}
				}
			}*/
		}
	}		
}
