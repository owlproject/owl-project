using System;

namespace OWL_Project
{
	public class Tuple<T1, T2, T3>
	{
	    public T1 First  { get; private set; }
	    public T2 Second { get; private set; }
		public T3 Tierce { get; private set; }
		
	    internal Tuple(T1 first, T2 second, T3 tierce)
	    {
	        First  = first;
	        Second = second;
			Tierce = tierce;
	    }
	}
	
	public static class Tuple
	{
	    public static Tuple<T1, T2, T3> Create<T1, T2, T3>(T1 first, T2 second, T3 tierce)
	    {
	        var tuple = new Tuple<T1, T2, T3>(first, second, tierce);
	        return tuple;
	    }
	}
}

