using System.Collections;
using System;

namespace OWL_Project
{
    public class Orb
    {
        /* Type de l'orbe (Feu, Terre, Or, ...) */
        protected int type;
        /* Orb's abscissa */
        protected int x;
        /* Orb's ordinate */
        protected int y;
        /* Orb's depth */
        protected int z;

        public Orb(int x, int y, int z)
        {
            this.type = -1;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /* Retourne le type de l'orbe */
        public int getType()
        {
            return this.type;
        }
        public int getX()
        {
            return x;
        }
        public int getY()
        {
            return y;
        }
        public int getZ()
        {
            return z;
        }

        public void setX(int x)
        {
            this.x = x;
        }
        public void setY(int y)
        {
            this.y = y;
        }
        public void setZ(int z)
        {
            this.z = z;
        }

		public void displayCoord()
		{
			Console.WriteLine("X = "+x+", Y = "+y+", Z = "+z);
		}
        public string TypeToString()
        {
            switch (type)
            {
				case -2: return "detruit";
				case -1: return "init";
                case 1: return "Feu";
                case 2: return "Eau";
                case 3: return "Terre";
                case 4: return "Air";
                case 5: return "Or";
                case 6: return "Xp";
                default: return null;
            }
        }

		public void detruit()
		{
			this.type = -2;
		}
		
		public bool estDetruit()
		{
			return type == -2;
		}

        public override string ToString()
        {
			if (this.type < 0 )
                return "Je suis une orbe de type \"" + TypeToString() + "\"";
			else if(this.type == -2)
				return "ERREUR : Je suis une orbe détruite";
            else
                return "ERREUR : Je suis une orbe sans type assigné";
        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
