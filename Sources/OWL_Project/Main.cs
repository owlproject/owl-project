/* IMPORTANT A LIRE AVANT MODIFICATION
 
 	Pour ouvrir le projet contenant ces sources, utiliser un éditeur comme monodevelop puis faites "ouvrir" --> OWL_Project.csproj
 	Cela permet de récupérer correctement les fichiers et leurs namespaces respectifs, et exécuter beaucoup plus simplement le code)
 	
 	Marc
  
*/

using System;
using System.Collections;
using System.Threading;
using System.Collections.Generic;

namespace OWL_Project
{
	class MainClass
	{
        public static void Main(string[] args)
        {
			int prod = 1; // 0 = en test ; 1 en production
			
			if (prod == 1)
			{
				Cube c = new Cube();
				c.init(5);
			
				//c.displayCouches();
//				c.displayPlateaux();
//
				Orb o1 = c.getOrb (0, 0, 0);
				Orb o2 = c.getOrb (2, 0, 0);
				Console.WriteLine("Nombre de combinaisons détectées avant d'avoir un cube stable : " + c.getCombinaisonsDetectees());
				
				//Test de retrouveFace
				/*List <int> result = c.retrouveFace(0, 0, 0);
				Console.WriteLine("Liste 1 :");
				foreach( int res in result)
					Console.Write("\tItem : "+ res+" ");
				c.getOrb(0, 0, 0).detruit();
				Console.WriteLine();
				
				result =  c.retrouveFace(0, 0, 1);
				Console.WriteLine("Liste 2 :");
				foreach( int res in result)
					Console.Write("\tItem : "+ res+" ");
				Console.WriteLine();
				
				result =  c.retrouveFace(0, 1, 0);
				Console.WriteLine("Liste 3 :");
				foreach( int res in result)
					Console.Write("\tItem : "+ res+" ");
				Console.WriteLine();
				
				c.getOrb(0, 1, 0).detruit();
				result =  c.retrouveFace(0, 1, 1);
				Console.WriteLine("Liste 4 :");
				foreach( int res in result)
					Console.Write("\tItem : "+ res+" ");
				Console.WriteLine();
				
				c.getOrb(0, 0, 1).detruit();
				result =  c.retrouveFace(0, 1, 1);
				Console.WriteLine("Liste 5 :");
				foreach( int res in result)
					Console.Write("\tItem : "+ res+" ");
				Console.WriteLine();*/
				
				//Test de permute plateau : face 1
					
				Plateau p1 = c.getPlateau(1);
				//Plateau p4 = c.getPlateau(4);
				//Plateau p5 = c.getPlateau(5);
				p1.display();
				//p4.display();
				//p5.display();
				c.permute(o1, o2);
				//c.permute_plateau(o1, o2);
				c.getFace (1);
				p1.display();
				//p4.display();
				//p5.display();
				
				//c.displayCouches();
			}
			else
			{
				int nbGen = 0;
				int nbRegen = 0;
				DateTime startTime = DateTime.Now;
				
				// Simulation de 500 génération aléatoire de cube 5x5
				for (int i=0; i < 500; i++)
				{
					Cube c = new Cube();
					c.init(5);
			
					nbGen++;
					nbRegen += c.getCombinaisonsDetectees();
					c.displayPlateaux();
				}
				
				// Simulation de 500 génération aléatoire de cube 7x7
				for (int i=0; i < 500; i++)
				{
					Cube c = new Cube();
					c.init(5);
			
					nbGen++;
					nbRegen += c.getCombinaisonsDetectees();
					c.displayPlateaux();
				}
				
				DateTime endTime = DateTime.Now;
				Console.WriteLine(nbGen + " cubes générées en " + (endTime.Millisecond - startTime.Millisecond) + "ms.");
				Console.WriteLine("Nombre de combinaisons détectées : " + nbRegen);
			}
			
			Console.WriteLine();
        }
	}
}