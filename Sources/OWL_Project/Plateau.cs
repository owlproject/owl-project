/* IMPORTANT A LIRE AVANT MODIFICATION
 
 	Pour ouvrir le projet contenant ces sources, utiliser un éditeur comme monodevelop puis faites "ouvrir" --> OWL_Project.csproj
 	Cela permet de récupérer correctement les fichiers et leurs namespaces respectifs, et exécuter beaucoup plus simplement le code
 	
 	Marc
  
*/

using System;
using System.Collections;
using System.Threading;

namespace OWL_Project
{
    public class Plateau
    {
        // Matrice de la face courante du cube
        private Orb[,] matrice;
        private int taille;
        private int face; //numero associe a la face du cube

        public Plateau(int ptaille, int pface)
        {
            this.taille = ptaille;
            this.face = pface;
            this.matrice = new Orb[taille, taille];
            //this.UpdateCases();
        }

        // Récupération de la matrice du plateau
        public Orb[,] getMatrice()
        {
            /*if (matrice == null)
                matrice = new matrice[taille]();*/
            return matrice;
        }

        // Set de la matrice du plateau
        public void setMatrice(Orb[,] pmatrice)
        {
            matrice = pmatrice;
        }

        //Getter for specific orb
        public Orb getOrb(int x, int y)
        {
            return matrice[x, y];
        }

        //Setter for specific orb
        public void setOrb(int x, int y, Orb o)
        {
            matrice[x, y] = o;
        }


        // Récupération de la matrice du plateau
        public int getFace()
        {
            return face;
        }

        // Set de la matrice du plateau
        public void setFace(int pface)
        {
            face = pface;
        }

        // actualisation des cases
		public void updateCases()//TODO : qu'est-ce qu'elle fait en fait ?
        {
            //attention, la permutation d'un coin doit prendre en compte les faces à qui l'orb appartient
            /**
                 * Remarques sur la recuperation des coins
                 * 
                 * face 1:: face: matrice = tab[0 ou taille][0 ou taille][0]
                 * face 2:: cote droit: matrice = tab[0 ou taille][taille][0 ou taille]
                 * face 3:: derriere: matrice = tab[0 ou taille][0 ou taille][taille]
                 * face 4:: cote gauche: matrice = tab[0 ou taille][0][0 ou taille]
                 * face 5:: dessus: matrice = tab[0][0 ou taille][0 ou taille]
                 * face 6:: dessous: matrice = tab[taille][0 ou taille][0 ou taille]
                 * 
                 * */


            // matrice = Cube.getInstance().getFaceCourante();
        }

        // Permutation de deux orbes
		public void permute(int x1, int y1, int x2, int y2, Orb o1, Orb o2)
        {
			setOrb(x1, y1, o2);
			setOrb(x2, y2, o1);
        }

        public string toString()
        {
            return "Je suis l'instance du plateau";
        }

		//Display Plateau instance
		public void display()
		{
			int i, j;
			for (i = 0; i < taille; i++) 
			{
				for (j = 0; j < taille; j++) 
				{
					Console.Write(getOrb (i, j).TypeToString () [0] + "|");
				}
				Console.WriteLine("");
			}
			Console.WriteLine("");
		}
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
