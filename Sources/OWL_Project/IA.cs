using System;
using System.Collections;
using System.Threading;
using System.Collections.Generic;

namespace OWL_Project
{
	public class IA
	{
		
		//Coup à jouer
		Orb orb1;
		Orb orb2;
		
		//Coordonnées
		int x1;
		int y1;
		int x2;
		int y2;
		
		//Impact du coup: nombre d'orb, ?, ?
		Tuple<int, int, int> impact;
		
		public IA ()
		{
			/* 
			 * quand attaque disponible -> BAM
			 * change de face quand y a plus rien sur le sien -> (pas d'orb de son élément ni d'attaque dispo)
			 * coup random quand rien de dispo
			 * */
		}
			
		/**
		 * Algorithme alphaBeta
		 * @param cube: cube de la partie
		 * @param profondeur : profondeur de l'arbre (à générer)
		 * @alpha : maximise le coup si vrai, minimisesinon
		 * @return : valeur du coup joué
		 * */
		int alphaBeta(Cube cube, int profondeur, int alpha)
		{
			return 0;
			//RQ : Boolean passer_tour = (x == 0)? true : false;
			
			/// coup à jouer : permute_plateau(Orb o1, Orb o2, int x1, int y1, int x2, int y2, int face)
			/*coup c;
			c.position = 10; //initialisation du coup
			c.couleur = Partie->joueurs[Partie->tour]->couleur;
		
			if(Niveau == 0 || passerTour(Partie)) //si la profondeur de l'arbre est atteinte ou que le joueur doit passer son tour
				return evaluation(Partie,passerTour(Partie)); //on retourne l'évaluation de la partie
		
			int meilleur_coup; //on initialise le meilleur coup
			while (!legal(Partie->othello, &c))
		    {
		        c.position+=1;
		    }
		
		    meilleur_coup=c.position;
		    c.position = 10; //initialisation du coup
			while(coupSuivant(Partie->othello, &c)) //tant qu'il y a des possibilités de coups (branches différentes)
			{
				jouerOrdi(Partie, &c); //le coup trouvé est joué
				int score=alphaBeta(Partie,Niveau-1,alpha,beta); //on regarde au niveau suivant
				annuler(Partie); //on annule le coup
				if (score>alpha) //on veut maximiser alpha
				{
					alpha=score;
					meilleur_coup=c.position; //on enregistre le nouveau meilleur coup trouvé
		
					if (alpha >= beta) //élagage
						break;
				}
			}
			Partie->listeCoups[Partie->CPT].position=meilleur_coup; //on ajoute la position du meilleur coup à la liste des coups
		
			return alpha;*/
		}
		
		/**
		 * Genere la liste des coups possibles
		 * @param cube: cube de la partie
		 * @return : liste des coordonnées des orbs à échanger pour jouer un coup possible
		 * */
		List<Tuple<int,int,String>> listeCoupsPossibles(Cube cube)
		{
			List<Tuple<int,int,String>> liste_coups = new List<Tuple<int,int,String>>();
			int variable =0;
			int dimension = 0;
			int taille = cube.getTaille();
			
			for(int x = 0; x < cube.getTaille(); x++)
			{
				for(int y = 0; y < cube.getTaille(); y++)
				{
					Orb o1 = cube.getPlateau(cube.getFaceCourante()).getOrb(x,y);
					for(int i = 0; i < 4; i++)
					{
						//reglage des variables dans le plateau
						variable = -2*(i%2) + 1;
						dimension = i / 2;
						
						//teste la permutation si possible
						if(x > 0 && y > 0 && x < taille - 1 && y < taille - 1)
						{
							//reglage des coordonnées du deuxieme orb
							int xx = 0, yy = 0;
							if( dimension == 0 ) xx = x + variable;
							else 				 yy = y + variable;
							Orb o2 = cube.getPlateau(cube.getFaceCourante()).getOrb(xx,yy);
							
							//permute
							cube.permute(o1, o2);
							
							//detecte si un coup est possible
							List<Orb> combi = cube.detecteCombinaison(x, y, cube.getFaceCourante());
							if(combi.Count != 0)
							{
								//construction du nouveau coup
								String s = "["+variable+"]["+dimension+"]";
								Tuple<int, int, String> new_tuple = Tuple.Create (x, y, s);
								liste_coups.Add(new_tuple);
							}
							
							//revient à l'état initial
							cube.permute(o2, o1);
						}						
					}
				}
			}
			
			return liste_coups;
		}
		
		/*
		 * Dit si un coup se trouve sur un coin du cube
		 * @param cube : cube de la partie
		 * @param coup : paramètres du coup
		 * @return : vrai si sur un coin, faux sinon
		 * */
		Boolean coins(Cube cube, Tuple<int,int,String> coup)
		{
			int taille = cube.getTaille();
			if(    (coup.First == 0      && coup.Second == 0) 
				|| (coup.First == taille && coup.Second == taille)
				|| (coup.First == 0      && coup.Second == taille) 
				|| (coup.First == taille && coup.Second == 0)) 
				return true;
			return false;
		}
		
		/*
		 * Nombre de coups possibles
		 * @param liste_coups : liste des coups possibles
		 * @return : nombre de coups possibles d'etre joués
		 * */
		int compteCoupsPossibles(List<Tuple<int,int,String>> liste_coups)
		{
			return liste_coups.Count ;
		}
		
		/*
		 * Evalue un coup à jouer
		 * @param cube : cube de la partie
		 * @param coup : paramètres du coup
		 * @return : valeur du coup
		 * */
		
		int evaluation(Cube cube, Tuple<int,int,String> coup)
		{
			return 0;
		/*
			int couleur=Partie->joueurs[Partie->tour]->couleur;
			int res,rep;
			if (Partie->situation) //si il s'agit d'une partie ordinateur contre ordinateur
				rep=Partie->tour;
			else //sinon l'ordinateur est toujours le joueur 0
				rep=0;
		
		    if (fin) //si le joueur doit passer son tour
			{
			    if (passerTour(Partie)) //si la partie est finie
			    	return materiel(Partie,Partie->joueurs[1]->couleur);
			    else
			        return evaluation(Partie,0); //sinon on évalue la situation
			}
			else if (Partie->othello->materiel[0]+Partie->othello->materiel[1]<20) //on évalue pour chaque phase de la partie (début, milieu et fin) grâce à la formule qui fait la somme des produits des coefficients avec leur critère
				res = Partie->coefs[rep][0]->coefDebut*mobilite(Partie,couleur)+Partie->coefs[rep][1]->coefDebut*materiel(Partie,couleur)+Partie->coefs[rep][2]->coefDebut*coins(Partie,couleur)+Partie->coefs[rep][3]->coefDebut*parite(Partie,couleur)+Partie->coefs[rep][4]->coefDebut*frontiere(Partie,couleur)+Partie->coefs[rep][5]->coefDebut*pionsDefinitifs(Partie,couleur);
		
			else if (Partie->othello->materiel[0]+Partie->othello->materiel[1]<40)
				res= Partie->coefs[rep][0]->coefMilieu*mobilite(Partie,couleur)+Partie->coefs[rep][1]->coefMilieu*materiel(Partie,couleur)+Partie->coefs[rep][2]->coefMilieu*coins(Partie,couleur)+Partie->coefs[rep][3]->coefMilieu*parite(Partie,couleur)+Partie->coefs[rep][4]->coefMilieu*frontiere(Partie,couleur)+Partie->coefs[rep][5]->coefMilieu*pionsDefinitifs(Partie,couleur);
		
			else //if (Partie->othello->materiel[0]+Partie->othello->materiel[1]<64)
				res= Partie->coefs[rep][0]->coefFin*mobilite(Partie,couleur)+Partie->coefs[rep][1]->coefFin*materiel(Partie,couleur)+Partie->coefs[rep][2]->coefFin*coins(Partie,couleur)+Partie->coefs[rep][3]->coefFin*parite(Partie,couleur)+Partie->coefs[rep][4]->coefFin*frontiere(Partie,couleur)+Partie->coefs[rep][5]->coefFin*pionsDefinitifs(Partie,couleur);
		
			return res;*/
		}
		
		/*
		 * Dit si un coup se trouve sur une arete du cube
		 * @param cube : cube de la partie
		 * @param coup : paramètres du coup
		 * @return : vrai si sur une arete, faux sinon
		 * */
		Boolean arete(Cube cube, Tuple<int,int,String> coup)
		{
			int taille = cube.getTaille();
			if(coup.First == 0 || coup.Second == 0 || coup.First == taille || coup.Second == taille) 
				return true;
			return false;		
		}
		
		/*
		 * Donne le nombre d'orb restant sur le cube après le coup
		 * @param cube : cube de la partie
		 * @param coup : paramètres du coup
		 * @return : nombre d'orb restants sur le cube
		 * */
		Boolean nombreOrb(Cube cube, Tuple<int,int,String> coup)
		{
			int taille = cube.getTaille();
			if(coup.First == 0 || coup.Second == 0 || coup.First == taille || coup.Second == taille) 
				return true;
			return false;		
		}
		
		/* Returne vrai si l'élément du coup est celui souhaité */
		public Boolean elementCoup(Cube cube, Tuple<int, int, String> coup)
		{
			// orb.getType() == ennemi.type
			return false;
		}
		
		/*  
		 * Calcul le mana gagné en effectuant ce coup
		 * */
		public int manaGagne(Cube cube, Tuple<int, int, String> coup)
		{
			/* mana récupéré par le coup */
			return 0;
		}
		
				
		/*
		 * Diférence entre le nombre de coups possibles pour le joueur et ceux possibles pour l'adversaire
		 * @param cube : cube de la partie
		 * @param coup : paramètres du coup
		 * @return : vrai si sur une arete, faux sinon
		 * */
		int mobilite(Tuple<int,int,String> coup_adv, Tuple<int,int,String> coup_joueur)
		{
			return 0;
		    //diférence entre le nombre de coups possibles pour le joueur et ceux possibles pour l'adversaire
			//return compteCoupsPossibles(coup_joueur)-compteCoupsPossibles(coup_adv);
		}
		
		/*
		 * Dit combien de points le prochain tour rapportera au joueur adverse
		 * @param cube : cube de la partie
		 * @param coup : paramètres du coup
		 * @return : nombre de points du coup suivant
		 * */
		int valeurCoupSuivant(Cube cube, Tuple<int,int,String> coup)
		{
			return 0;
		}
		
		/*
		 * Dit si un coup rapporte un type d'orb en majorité  
		 * @param cube : cube de la partie
		 * @param coup : paramètres du coup
		 * @return : type de l'orb majoritairement gagné
		 * */
		int maxOrbMemeType(Cube cube, Tuple<int,int,String> coup)
		{
			return 0;
			//List<Orb> listeorb = detecteCompatibilite(coup.item1 , coup.item2 , cube.getFaceCourante(), int variable, int dimension, List<Orb> result)
		}
		
		/*
		 * Dit si un coup empechera l'autre joueur de jouer
		 * @param cube : cube de la partie
		 * @param coup : paramètres du coup
		 * @return : vrai si le coup empechera l'autre joueur de jouer, faux sinon
		 * */
		Boolean empecheProchainCoup(Cube cube, Tuple<int,int,String> coup)
		{
			return false;
		}

	}
}


