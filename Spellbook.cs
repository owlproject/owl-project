
using UnityEngine;
using System.Collections;
using RPG;
using Interface;

public class Spellbook : MonoBehaviour {
	
	public GameObject open;
	public GameObject window;
	public ArrayList<GameObject> spells;
	public GameObject close;
		
	void Start() {
		ArrayList<Spell> spells = GameObject.Find("InfoCombat").GetComponent<LoadPlayerInfos>().player.spells;
		
		
		open = GameObject.Find();
	}
		
	void  Update () {
		GameObject clickedGmObj = null;
		
		if(Input.GetMouseButtonUp(0)) {
	       	clickedGmObj = GetClickedGameObject();
	    } else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
	    	clickedGmObj = GetTouchedGameObject();
	    }
	
	    
	    if(clickedGmObj == open) {
			render(true);	
	    }
		else if(clickedGmObj == close) {
			render(false);
		}
	    
	}
	
	void renderObject(bool show) {
		
		/* MeshCollider cMesh = (MeshCollider) open.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		MeshRenderer pMesh = (MeshRenderer) open.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane1.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane1.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane2.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane2.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane3.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane3.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane4.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane4.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane5.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane5.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) plane6.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) plane6.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show;
		
		cMesh = (MeshCollider) closePlane.GetComponent( typeof( MeshCollider) );
		cMesh.enabled = show;
		pMesh = (MeshRenderer) closePlane.GetComponent( typeof( MeshRenderer) );
		pMesh.enabled = show; */
	}

	GameObject GetClickedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit	
		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
	
	GameObject GetTouchedGameObject (){
	    // Builds a ray from camera point of view to the mouse position
	    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
	    RaycastHit hit;
	    // Casts the ray and get the first game object hit
	    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
	       return hit.transform.gameObject;
	    else
	       return null;
	}
}